﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using UdpKit;
using Audio;
using TMPro;

public class GameSceneControl : Bolt.GlobalEventListener {

    //Types

    /// <summary>
    /// Token used to send updated flags to clients after they are triggered
    /// </summary>
    public class GameFlagsToken : Bolt.IProtocolToken
    {
        public bool Blue10;
        public bool Blue50;
        public bool BlueVictory;
        public bool Red10;
        public bool Red50;
        public bool RedVictory;
        public bool Draw;
        public bool TriggeredEnding;

        public void Read(UdpPacket packet)
        {
            Blue10 = packet.ReadBool();
            Blue50 = packet.ReadBool();
            BlueVictory = packet.ReadBool();
            Red10 = packet.ReadBool();
            Red50 = packet.ReadBool();
            RedVictory = packet.ReadBool();
            Draw = packet.ReadBool();
            TriggeredEnding = packet.ReadBool();
        }
        public void Write(UdpPacket packet)
        {
            packet.WriteBool(Blue10);
            packet.WriteBool(Blue50);
            packet.WriteBool(BlueVictory);
            packet.WriteBool(Red10);
            packet.WriteBool(Red50);
            packet.WriteBool(RedVictory);
            packet.WriteBool(Draw);
            packet.WriteBool(TriggeredEnding);
        }
    }

    //Global Handle
    static GameSceneControl _singleton;

    //References
    [Header("References")]
    [SerializeField] Field _planetField;
    [SerializeField] GameObject _gameplayObjects;
    [SerializeField] GameObject _redPlayerWins;
    [SerializeField] GameObject _bluePlayerWins;
    [SerializeField] HealthBar[] _healthBars;
    [SerializeField] ColourObject[] _planetColours;
    [SerializeField] PlanetSpawner _spawner;
    [SerializeField] UISlideInOut _uiSlider;
    [SerializeField] TextMeshProUGUI _gameTimer;

    public static Field PlanetField
    {
        get { return _singleton._planetField; }
    }
    public static ColourObject[] Colours
    {
        get { return _singleton._planetColours; }
    }
    public static HealthBar GetHealthBar(int player)
    {
        if(player < 2)
        {
            return _singleton._healthBars[player];
        }
        else
        {
            return null;
        }
    }

    [Header("Asset references")]
    [SerializeField] GameObject _explosionPrefab;

    [Header("Properties")]
    [SerializeField] float _banterTriggerTime = 15f;

    //Flags
    public static bool Blue50;
    public static bool Blue10;
    public static bool Red50;
    public static bool Red10;
    public static bool BlueVictory;
    public static bool RedVictory;
    public static bool Draw;
    public static bool TriggeredEnding;

    public static UnityEvent GameEnd { get; private set; }
    float _startTime;

    private void Awake()
    {
        _singleton = this;

        Blue50 = false;
        Blue10 = false;
        Red50 = false;
        Red10 = false;
        BlueVictory = false;
        RedVictory = false;
        Draw = false;
        TriggeredEnding = false;

        GameEnd = new UnityEvent();
    }
    private void Start()
    {
        MusicPlayer.PlayTrack(Music.GAMEPLAY_1);
        VoicePlayer.PlayTrack(VoiceType.START);

        _startTime = Time.time;
        GameStatistics.GameStats.CreateNewLists();

        if(BoltNetwork.IsRunning)
        {
            if(PlayerList.BluePlayer.IsLocalPlayer)
            {
                GameObject g = BoltNetwork.Instantiate(BoltPrefabs.Player, new Vector3(-3f, 1f, 0f), Quaternion.Euler(new Vector3(0f, 180f, 0f)));
                g.GetComponent<PlayerControl>().state.PlayerNum = 0;
            }
            else if(PlayerList.RedPlayer.IsLocalPlayer)
            {
                GameObject g = BoltNetwork.Instantiate(BoltPrefabs.Player, new Vector3(3f, -2f, 0f), Quaternion.Euler(new Vector3(0f, 180f, 0f)));
                g.GetComponent<PlayerControl>().state.PlayerNum = 1;
            }
        }
    }

    /// <summary>
    /// On escape key press quit game
    /// </summary>
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if(BoltNetwork.IsRunning)
            {
                if(PlayerList.IsLocalPlaying || BoltNetwork.IsServer)
                {
                    QuitGame.Create().Send();
                }
                else
                {
                    BoltNetwork.Server.Disconnect();
                }
            }
            else
            {
                QuitGameScene();
            }
        }
    }
    /// <summary>
    /// Trigger tidbit voice lines when silent for a while
    /// </summary>
    private void FixedUpdate()
    {
        if(VoicePlayer.SilentTime > _banterTriggerTime)
        {
            VoicePlayer.PlayTrack(VoiceType.BANTER);
        }

        float gameTime = Time.time - _startTime;
        System.TimeSpan timeSpan = System.TimeSpan.FromSeconds(gameTime);
        _gameTimer.text = string.Format("{0:00}:{1:00}", timeSpan.Minutes, timeSpan.Seconds);
    }

    //Flag events

    public static void TriggerBlueHealth50()
    {
        if(!BoltNetwork.IsRunning || BoltNetwork.IsServer)
        {
            _singleton.BlueHealth50();
        }
    }
    public static void TriggerBlueHealth10()
    {
        if (!BoltNetwork.IsRunning || BoltNetwork.IsServer)
        {
            _singleton.BlueHealth10();
        }
    }
    public static void TriggerRedHealth50()
    {
        if (!BoltNetwork.IsRunning || BoltNetwork.IsServer)
        {
            _singleton.RedHealth50();
        }
    }
    public static void TriggerRedHealth10()
    {
        if (!BoltNetwork.IsRunning || BoltNetwork.IsServer)
        {
            _singleton.RedHealth10();
        }
    }
    public static void TriggerBlueWin()
    {
        if (!BoltNetwork.IsRunning || BoltNetwork.IsServer)
        {
            _singleton.BlueWin();
        }
    }
    public static void TriggerRedWin()
    {
        if (!BoltNetwork.IsRunning || BoltNetwork.IsServer)
        {
            _singleton.RedWin();
        }
    }
    public static void TriggerGameEnd()
    {
        if (!BoltNetwork.IsRunning || BoltNetwork.IsServer)
        {
            _singleton.OnGameEnd();
        }
    }

    private void BlueHealth50()
    {
        if (!Blue50)
        {
            Blue50 = true;

            MusicStage2();
            VoicePlayer.PlayTrack(VoiceType.BLUE_HEALTH_50);

            SendFlagUpdate();
        }
    }
    private void BlueHealth10()
    {
        if(!Blue10)
        {
            Blue10 = true;
            Blue50 = true;

            MusicStage3();
            VoicePlayer.PlayTrack(VoiceType.BLUE_HEALTH_10);
            SendFlagUpdate();
        }
    }
    private void RedHealth50()
    {
        if (!Red50)
        {
            Red50 = true;

            MusicStage2();
            VoicePlayer.PlayTrack(VoiceType.RED_HEALTH_50);
            SendFlagUpdate();
        }
    }
    private void RedHealth10()
    {
        if (!Red10)
        {
            Red10 = true;
            Red50 = true;

            MusicStage2();
            VoicePlayer.PlayTrack(VoiceType.RED_HEALTH_10);
            SendFlagUpdate();
        }
    }
    private void BlueWin()
    {
        if(!BlueVictory && !RedVictory)
        {
            BlueVictory = true;
            Red10 = true;
            Red50 = true;

            TriggerGameEnd();
            GameStatistics.GameStats.SetScore(GameStatistics.GameStats.IntType.WINNER, 0);
            SendFlagUpdate();
            
            if(BoltNetwork.IsRunning)
            {
                PlayerList.BluePlayer.Wins += 1;
                PlayerList.RedPlayer.Losses += 1;
                PlayerList.SyncPlayerList();
            }

            ScoreScreenController.WinnerFinalHealth = PlayerHealth.PlayerHealths[0].Fraction;
        }
    }
    private void RedWin()
    {
        if (!BlueVictory && !RedVictory)
        {
            RedVictory = true;
            Blue10 = true;
            Blue50 = true;

            TriggerGameEnd();
            GameStatistics.GameStats.SetScore(GameStatistics.GameStats.IntType.WINNER, 1);
            SendFlagUpdate();

            if(BoltNetwork.IsRunning)
            {
                PlayerList.RedPlayer.Wins += 1;
                PlayerList.BluePlayer.Losses += 1;
                PlayerList.SyncPlayerList();
            }

            ScoreScreenController.WinnerFinalHealth = PlayerHealth.PlayerHealths[1].Fraction;
        }
    }
    private void OnGameEnd()
    {
        if (!TriggeredEnding)
        {
            TriggeredEnding = true;

            GameSceneExitTransition();
            _spawner.gameObject.SetActive(false);

            GameStatistics.GameStats.SetScore(GameStatistics.GameStats.FloatType.GAME_LENGTH, Time.time - _startTime);
            GameEnd.Invoke();

            MusicEnd();

            _singleton.StartCoroutine(_singleton.CallMethodAfterWait(_singleton.LoadScoreScreen,5f));

            if(BoltNetwork.IsRunning && BoltNetwork.IsServer)
            {
                GameStatistics.GameStats.ShareStats();
            }
        }
    }


    //Flag syncing

    /// <summary>
    /// Send a GameFlagsToken to update the flags on clients
    /// </summary>
    private void SendFlagUpdate()
    {
        if(BoltNetwork.IsRunning && BoltNetwork.IsServer)
        {
            GameFlagUpdate evnt = GameFlagUpdate.Create();
            GameFlagsToken token = new GameFlagsToken();

            token.Blue10 = Blue10;
            token.Blue50 = Blue50;
            token.BlueVictory = BlueVictory;
            token.Red10 = Red10;
            token.Red50 = Red50;
            token.RedVictory = RedVictory;
            token.TriggeredEnding = TriggeredEnding;
            token.Draw = Draw;

            evnt.FlagToken = token;
            evnt.Send();
        }
    }
    /// <summary>
    /// On client replace flags with those of server
    /// </summary>
    public override void OnEvent(GameFlagUpdate evnt)
    {
        if(!BoltNetwork.IsServer)
        {
            GameFlagsToken token = (GameFlagsToken)evnt.FlagToken;

            if (!TriggeredEnding && token.TriggeredEnding) GameSceneExitTransition();

            Blue10 = token.Blue10;
            Blue50 = token.Blue50;
            BlueVictory = token.BlueVictory;
            Red10 = token.Red10;
            Red50 = token.Red50;
            RedVictory = token.RedVictory;
            TriggeredEnding = token.TriggeredEnding;
            Draw = token.Draw;

            UpdateMusic();
        }
    }
    /// <summary>
    /// Update the music depending on the flags that have been set
    /// </summary>
    private void UpdateMusic()
    {
        if(TriggeredEnding)
        {
            MusicEnd();
        }
        else if(Blue10 || Red10)
        {
            MusicStage3();
        }
        else if(Blue50 || Red50)
        {
            MusicStage2();
        }
    }


    //Game end transition

    //
    public void GameSceneExitTransition()
    {
        _uiSlider.Exit();
        StartCoroutine(AllPlanetDestruction());
    }
    //
    IEnumerator AllPlanetDestruction()
    {
        float period = 0.6f;
        float reduction = 0.75f;
        float duration = 0f;

        if(!BoltNetwork.IsRunning || BoltNetwork.IsServer)
        {
            while (PlanetControl.PlanetList.Count > 0)
            {
                PlanetControl.PlanetList[0].CollisionComponent.DestroyPlanet();
                yield return new WaitForSeconds(period);
                period *= reduction;
                duration += Time.deltaTime;
            }
        }

        float waitTime = Mathf.Max(0f, 2f - duration);
        yield return new WaitForSeconds(waitTime);
        
        if ((PlayerControl.BluePlayer != null && PlayerControl.BluePlayer.Alive) && (!BoltNetwork.IsRunning || PlayerControl.BluePlayer.entity.isOwner))
        {
            PlayerControl.BluePlayer.PlayerDeath();
        }
        
        if ((PlayerControl.RedPlayer != null && PlayerControl.RedPlayer.Alive) && (!BoltNetwork.IsRunning || PlayerControl.RedPlayer.entity.isOwner))
        {
            PlayerControl.RedPlayer.PlayerDeath();
        }
        
    }


    //Music methods

    /// <summary>
    /// Fade in tracks for second stage
    /// </summary>
    public static void MusicStage2()
    {
        MusicPlayer.FadeInTrack(Music.GAMEPLAY_2);
    }
    /// <summary>
    /// Fade in tracks for third stage
    /// </summary>
    public static void MusicStage3()
    {
        MusicPlayer.FadeInTrack(Music.GAMEPLAY_2);
        MusicPlayer.FadeInTrack(Music.GAMEPLAY_3);
    }
    /// <summary>
    /// Fade out tracks at end
    /// </summary>
    public static void MusicEnd()
    {
        MusicPlayer.FadeOutAll();
    }


    //Planet destruction

    /// <summary>
    /// Create an explosion with the given parameters
    /// </summary>
    public static void CreateExplosion(Vector3 position, Vector3 velocity, float scale)
    {
        GameObject obj = Instantiate(_singleton._explosionPrefab, position, Random.rotation);
        obj.transform.localScale = new Vector3(scale, scale, scale);
        obj.GetComponent<ExplodingPlanet>().Explode(velocity);
    }
    /// <summary>
    /// Call CreateExplosion on planetdestruction event
    /// </summary>
    public override void OnEvent(PlanetDestruction evnt)
    {
        CreateExplosion(evnt.Position, evnt.Velocity, evnt.Scale);
    }


    //Delay coroutine

    /// <summary>
    /// Call a no-parameter method after a wait
    /// </summary>
    private IEnumerator CallMethodAfterWait(System.Action method, float delayInSeconds)
    {
        yield return new WaitForSeconds(delayInSeconds);

        method.Invoke();
    }


    //Connect/Disconnect stuff

    /// <summary>
    /// Send flag update when spectator joins
    /// </summary>
    public override void Connected(BoltConnection connection)
    {
        SendFlagUpdate();
        GameSettings.SendSettingsToClient();
    }
    /// <summary>
    /// When recieving quit event, server loads back into the lobby
    /// </summary>
    public override void OnEvent(QuitGame evnt)
    {
        VoicePlayer.Stop();

        if (BoltNetwork.IsServer)
        {
            BoltLoadLobby();
        }
    }
    /// <summary>
    /// Server will load to lobby if player leaves, client will disconnect to online menu
    /// </summary>
    public override void Disconnected(BoltConnection connection)
    {
        VoicePlayer.Stop();

        if (BoltNetwork.IsServer)
        {
            PlayerList.Player disconnectedPlayer = PlayerList.GetPlayer(connection);

            if (PlayerList.BluePlayer == disconnectedPlayer || PlayerList.RedPlayer == disconnectedPlayer)
            {
                BoltLoadLobby();
            }
        }
        else
        {
            StaticUIEffects.SendNotification("Disconnected from host, click to return to menu.", methodOnDismissal: LocalLoadOnlineMenu);
        }

    }


    //Load scenes

    //
    public void QuitGameScene()
    {
        _spawner.gameObject.SetActive(false);
        GameSceneExitTransition();
        if (!BoltNetwork.IsRunning)
        {
            StartCoroutine(CallMethodAfterWait(OfflineLoadMenu, 5f));
        }
        else if(BoltNetwork.IsServer)
        {
            StartCoroutine(CallMethodAfterWait(BoltLoadLobby, 5f));
        }
        else
        {
            //Client leave option

            //StartCoroutine(CallMethodAfterWait(BoltLoadLobby, 5f));
        }
    }
    //
    public void OfflineLoadMenu()
    {
        SceneManager.LoadScene("MenuScene");
    }
    /// <summary>
    /// Loads the online menu locally
    /// </summary>
    public void LocalLoadOnlineMenu()
    {
        SceneManager.LoadScene("OnlineMenu");
    }
    /// <summary>
    /// Loads all clients back into the lobby scene
    /// </summary>
    public void BoltLoadLobby()
    {
        _spawner.gameObject.SetActive(false);
        BoltNetwork.LoadScene("Lobby");
    }
    /// <summary>
    /// Loads into the score screen
    /// </summary>
    public void LoadScoreScreen()
    {
        if (!BoltNetwork.IsRunning)
        {
            SceneManager.LoadScene("ScoreScreen");
        }
        else if (BoltNetwork.IsServer)
        {
            _spawner.gameObject.SetActive(false);
            PlayerList.ClearPlayerSlots();
            BoltNetwork.LoadScene("ScoreScreen");
        }
    }

}
