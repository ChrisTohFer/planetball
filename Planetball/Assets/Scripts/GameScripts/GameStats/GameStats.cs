﻿using System.Collections;
using System.Collections.Generic;
using UdpKit;
using UnityEngine;

namespace GameStatistics
{

    //Class keeps track of various in game statistics to be shown on the end game screen
    public class GameStats : MonoBehaviour
    {

        //Singleton reference

        static GameStats _singleton;
        
        //Types

        //Enum for float statistics
        public enum FloatType
        {
            GAME_LENGTH,

            BLUE_BIGGEST_HIT,
            BLUE_BIGGEST_HIT_TAKEN,
            BLUE_LONGEST_STREAK,
            BLUE_SELF_DAMAGE,

            RED_BIGGEST_HIT,
            RED_BIGGEST_HIT_TAKEN,
            RED_LONGEST_STREAK,
            RED_SELF_DAMAGE,

            FLOAT_TYPE_MAX
        }
        //Enum for int statistics
        public enum IntType
        {
            WINNER,

            BLUE_LONGEST_COMBO,
            BLUE_OWN_GOALS,

            RED_LONGEST_COMBO,
            RED_OWN_GOALS,

            INT_TYPE_MAX
        }


        //Variables

        float[] _floatValues;
        int[] _intValues;


        //Initialisation

        /// <summary>
        /// Initialise singleton reference + dontdestroyonload
        /// </summary>
        private void Awake()
        {
            if(_singleton == null)
            {
                _singleton = this;
                DontDestroyOnLoad(gameObject);
            }
        }
        /// <summary>
        /// Destroy if there is already another GameStats object
        /// </summary>
        private void Start()
        {
            if(_singleton != this)
            {
                Destroy(gameObject);
            }
        }


        //Stats access

        /// <summary>
        /// Return the value of the float stat
        /// </summary>
        public static float GetStat(FloatType type)
        {
            return _singleton._floatValues[(int)type];
        }
        /// <summary>
        /// Return the value of the int stat
        /// </summary>
        public static int GetStat(IntType type)
        {
            return _singleton._intValues[(int)type];
        }


        //Stats editing

        /// <summary>
        /// Creates new empty lists for stats
        /// </summary>
        public static void CreateNewLists()
        {
            _singleton._floatValues = new float[(int)FloatType.FLOAT_TYPE_MAX];
            _singleton._intValues = new int[(int)IntType.INT_TYPE_MAX];
        }
        
        /// <summary>
        /// Check if value is greater than current value for a float stat and replace if true
        /// </summary>
        public static void SubmitNewScore(FloatType type, float value)
        {
            if(value > _singleton._floatValues[(int)type])
            {
                _singleton._floatValues[(int)type] = value;
            }
        }
        /// <summary>
        /// Check if value is greater than current value for an int stat and replace if true
        /// </summary>
        public static void SubmitNewScore(IntType type, int value)
        {
            if (value > _singleton._intValues[(int)type])
            {
                _singleton._intValues[(int)type] = value;
            }
        }
        /// <summary>
        /// Add value to float stat
        /// </summary>
        public static void AddToScore(FloatType type, float value)
        {
            _singleton._floatValues[(int)type] += value;
        }
        /// <summary>
        /// Add value to int stat
        /// </summary>
        public static void AddToScore(IntType type, int value)
        {
            _singleton._intValues[(int)type] += value;
        }
        /// <summary>
        /// Set the value of a float stat
        /// </summary>
        public static void SetScore(FloatType type, float value)
        {
            _singleton._floatValues[(int)type] = value;
        }
        /// <summary>
        /// Set the value of an int stat
        /// </summary>
        public static void SetScore(IntType type, int value)
        {
            _singleton._intValues[(int)type] = value;
        }


        //Stats syncing

        //Token for sharing stats
        public class GameStatsToken : Bolt.IProtocolToken
        {
            public float[] FloatStats;
            public int[] IntStats;

            public void Read(UdpPacket packet)
            {
                int count = packet.ReadInt();
                FloatStats = new float[count];
                for(int i = 0; i < count; ++i)
                {
                    FloatStats[i] = packet.ReadFloat();
                }

                count = packet.ReadInt();
                IntStats = new int[count];
                for (int i = 0; i < count; ++i)
                {
                    IntStats[i] = packet.ReadInt();
                }
            }
            public void Write(UdpPacket packet)
            {
                int count = FloatStats.Length;
                packet.WriteInt(count);
                for(int i = 0; i < count; ++i)
                {
                    packet.WriteFloat(FloatStats[i]);
                }

                count = IntStats.Length;
                packet.WriteInt(count);
                for (int i = 0; i < count; ++i)
                {
                    packet.WriteInt(IntStats[i]);
                }
            }
        }

        /// <summary>
        /// Send out the game stats to other clients
        /// </summary>
        public static void ShareStats()
        {
            GameStatsToken token = new GameStatsToken();
            token.FloatStats = _singleton._floatValues;
            token.IntStats = _singleton._intValues;

            SyncGameStats evnt = SyncGameStats.Create();
            evnt.GameStatsToken = token;

            evnt.Send();
        }
        /// <summary>
        /// Clients will apply stats recieved from the host
        /// </summary>
        public static void RecieveSyncGameStatsEvent(SyncGameStats evnt)
        {
            if (!BoltNetwork.IsServer)
            {
                GameStatsToken token = (GameStatsToken)evnt.GameStatsToken;

                _singleton._floatValues = token.FloatStats;
                _singleton._intValues = token.IntStats;
            }
        }

    }

}