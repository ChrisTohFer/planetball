﻿using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UdpKit;

public class ScoreScreenController : Bolt.GlobalEventListener {

    //References
    [SerializeField] GameObject[] _victoryIcons;
    [SerializeField] UISlideInOut _uiSlider;

    public static float WinnerFinalHealth;


    //Initialisation

    /// <summary>
    /// Stats are tracked via statics, class only instantiated on stats screen for references to UI
    /// </summary>
    private void Start()
    {
        Audio.MusicPlayer.PlayTrack(Audio.Music.MENU);

        int winner = GameStatistics.GameStats.GetStat(GameStatistics.GameStats.IntType.WINNER);

        if (winner == 0 || winner == 1) { _victoryIcons[winner].SetActive(true); }
        
        if(winner == 0)
        {
            if(WinnerFinalHealth >= 1f)
            {
                Audio.VoicePlayer.PlayInSeconds(Audio.VoiceType.BLUE_WIN_PERFECT, 2f);
            }
            else
            {
                Audio.VoicePlayer.PlayInSeconds(Audio.VoiceType.BLUE_WIN, 2f);
            }
        }
        else if(winner == 1)
        {
            if (WinnerFinalHealth >= 1f)
            {
                Audio.VoicePlayer.PlayInSeconds(Audio.VoiceType.RED_WIN_PERFECT, 2f);
            }
            else
            {
                Audio.VoicePlayer.PlayInSeconds(Audio.VoiceType.RED_WIN, 2f);
            }
        }
    }

    
    //Quitting

    /// <summary>
    /// Load lobby or main menu
    /// </summary>
    public void LeaveScoreScreen()
    {
        if (BoltNetwork.IsRunning)
        {
            SceneManager.LoadScene("Lobby");
        }
        else
        {
            SceneManager.LoadScene("MenuScene");
        }
    }


    //Disconnect stuff
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !_uiSlider.Animating)
        {
            _uiSlider.Exit();
            _victoryIcons[GameStatistics.GameStats.GetStat(GameStatistics.GameStats.IntType.WINNER)].GetComponent<UISlideInOut>().Exit();

            /*if (BoltNetwork.IsRunning)
            {
                //QuitGame.Create().Send();
                Audio.VoicePlayer.Stop();
                SceneManager.LoadScene("Lobby");
            }
            else
            {
                SceneManager.LoadScene(0);
            }*/
        }
    }

    public override void OnEvent(QuitGame evnt)
    {
        Audio.VoicePlayer.Stop();

        if (BoltNetwork.IsServer)
        {
            BoltNetwork.LoadScene("Lobby");
        }
    }

    public override void Disconnected(BoltConnection connection)
    {
        Audio.VoicePlayer.Stop();
    }

    //Load scenes

    //
    public void LocalLoadOnlineMenu()
    {
        SceneManager.LoadScene("OnlineMenu");
    }
    //
    public void BoltLoadLobby()
    {
        BoltNetwork.LoadScene("Lobby");
    }

}
