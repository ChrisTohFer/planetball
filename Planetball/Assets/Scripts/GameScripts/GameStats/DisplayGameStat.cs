﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DisplayGameStat : MonoBehaviour {

    //Enums

    public enum StatType
    {
        FLOAT,
        INT
    }

    //References

    [Header("Component References")]
    [SerializeField] TextMeshProUGUI _textObject;

    //Properties

    [Header("Properties")]
    [SerializeField] StatType _type;
    [SerializeField] GameStatistics.GameStats.FloatType _floatType;
    [SerializeField] GameStatistics.GameStats.IntType _intType;
    [SerializeField] string _prefix;
    [SerializeField] string _suffix;
    [SerializeField] double _scaleFactor = 1.0;
    [SerializeField] bool _timeFormat = false;

    //Initialisation

    private void Start()
    {
        _textObject.text = _prefix + ValueString() + _suffix;
    }


    private string ValueString()
    {
        if(_type == StatType.FLOAT)
        {
            if(_timeFormat)
            {
                float timeInSeconds = GameStatistics.GameStats.GetStat(_floatType);
                System.TimeSpan timeSpan = System.TimeSpan.FromSeconds(timeInSeconds);
                return string.Format("{0:00}:{1:00}", timeSpan.Minutes, timeSpan.Seconds);
            }
            else
            {
                double value = _scaleFactor * GameStatistics.GameStats.GetStat(_floatType);
                value = System.Math.Round(value);
                return string.Format("{0:#,0}", value);
            }
        }
        else if(_type == StatType.INT)
        {
            int value = (int)_scaleFactor * GameStatistics.GameStats.GetStat(_intType);
            value = Mathf.RoundToInt(value);
            return string.Format("{0:#,0}", value);
        }

        return null;
    }

}
