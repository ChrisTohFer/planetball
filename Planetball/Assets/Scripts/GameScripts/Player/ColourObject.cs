﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewColours", menuName = "ColourObject")]
public class ColourObject : ScriptableObject {

    [SerializeField] Material _playerMaterial;
    [SerializeField] Color _particlePassiveColor;
    [SerializeField] Color _particleActiveColor;

    public void Apply(GameObject g)
    {
        g.GetComponentInChildren<Renderer>().material = _playerMaterial;
        AttractionParticlesController particles = g.GetComponent<AttractionController>().Particles;

        particles.PParticleColor = _particlePassiveColor;
        particles.AParticleColor = _particleActiveColor;
    }

}
