﻿using UnityEngine;

//Script causes gameobject to follow a specifict object and look at another while active

public class PinTransform : MonoBehaviour {

    public bool Active;
    public Transform LocationPin;
    public Transform RotationPin;

    private void FixedUpdate()
    {
        if(Active)
        {
            if(LocationPin == null || RotationPin == null)
            {
                Active = false;
            }
            else
            {
                transform.position = LocationPin.position;
                transform.LookAt(RotationPin);
            }
        }
    }

    /// <summary>
    /// Sets the location pin
    /// </summary>
    public void PinLocationToTransform(Transform t)
    {
        LocationPin = t;
    }

}
