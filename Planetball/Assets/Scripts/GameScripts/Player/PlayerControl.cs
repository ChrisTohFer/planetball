﻿using UnityEngine;

//Central class for player prefab, contains references to major components and some core functionality
public class PlayerControl : Bolt.EntityBehaviour<IPlayer> {

    //Global references
    public static PlayerControl BluePlayer;
    public static PlayerControl RedPlayer;

    //Variables
    [SerializeField] int _playerNum = 0;

    bool _alive = true;
    public bool Alive
    {
        get { return _alive; }
    }

    public int PlayerNum
    {
        get
        {
            if(BoltNetwork.IsConnected)
            {
                return state.PlayerNum;
            }
            else
            {
                return _playerNum;
            }
        }
    }

    //References
    [Header("Component references")]
    [SerializeField] Rigidbody _rigidbody;
    [SerializeField] PlayerMovement _movement;
    [SerializeField] PlayerHealth _health;
    [SerializeField] AttractionController _attraction;
    [SerializeField] Transform _meshTransform;

    [Header("Asset references")]
    [SerializeField] GameObject _explosion;

    public PlayerMovement Movement
    {
        get { return _movement; }
    }
    public PlayerHealth Health
    {
        get { return _health; }
    }
    public AttractionController Attraction
    {
        get { return _attraction; }
    }

    //Initialisation

    /// <summary>
    /// Set global reference if not online (otherwise set in attached)
    /// </summary>
    private void Start()
    {
        if(!BoltNetwork.IsRunning)
        {
            if(PlayerNum == 0)
            {
                BluePlayer = this;
            }
            else if(PlayerNum == 1)
            {
                RedPlayer = this;
            }
        }
    }
    /// <summary>
    /// Set tracked properties and properties that depend on owner
    /// </summary>
    public override void Attached()
    {
        state.SetTransforms(state.PlayerTransform, transform, _meshTransform);
        state.AddCallback("PlayerNum", OnPlayerNumChanged);

        if(!entity.isOwner)
        {
            OnPlayerNumChanged();
            if (!PlayerList.IsLocalPlaying)
            {
                Health.SetHealthToStateHealth();
            }
        }

        if (!entity.isOwner)
        {
            _rigidbody.isKinematic = true;
        }

        if (entity.isOwner)
        {
            if (PlayerList.BluePlayer.IsLocalPlayer)
            {
                state.PlayerNum = 0;
                BluePlayer = this;
            }
            else if(PlayerList.RedPlayer.IsLocalPlayer)
            {
                state.PlayerNum = 1;
                RedPlayer = this;
            }
        }

        //Apply team colours to prefab
        GameSceneControl.Colours[state.PlayerNum].Apply(gameObject);
    }

    /// <summary>
    /// Cleanup
    /// </summary>
    public override void Detached()
    {
        Attraction.DestroyParticlesObject();
    }
    private void OnDisable()
    {
        if(!BoltNetwork.IsRunning)
        {
            Attraction.DisableParticlesObject();
        }
    }

    //Property callbacks
    private void OnPlayerNumChanged()
    {
        _playerNum = state.PlayerNum;
        _health.Bar = GameSceneControl.GetHealthBar(_playerNum);
    }

    //Methods

    /// <summary>
    /// Called when health reaches 0
    /// </summary>
    public void PlayerDeath()
    {
        //Disable planet
        GetComponentInChildren<Renderer>().enabled = false;
        GetComponentInChildren<Collider>().enabled = false;
        Attraction.DisableParticlesObject();
        
        //Create explosion
        GameSceneControl.CreateExplosion(transform.position, _rigidbody.velocity, transform.localScale.x);

        //Set alive to false
        _alive = false;
    }


}
