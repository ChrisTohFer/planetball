﻿using UnityEngine;
using Audio;
using GameStatistics;

//Class tracks the health of the player
public class PlayerHealth : Bolt.GlobalEventListener {

    //Global list

    public static PlayerHealth[] PlayerHealths;

    //References
    [Header("References")]
    [SerializeField] PlayerControl _controller;
    [SerializeField] HealthBar _healthBar;

    public HealthBar Bar
    {
        get { return _healthBar; }
        set { _healthBar = value; }
    }

    //Variables
    [Header("Variables")]
    [SerializeField] float _sizeDamagePowerScaling = 1.5f;
    [SerializeField] float _bigHitThreshold = 50f;
    [SerializeField] float _dodgeLineTriggerTime = 30f;

    float _maxHealth;
    float _health;
    float _lastHitTime;
    bool _lineTriggered = false;
    static int[] _comboCounters;

    public float Health
    {
        get { return _health; }
        set {
            if (BoltNetwork.IsRunning && _controller.entity.isOwner) _controller.state.Health = value;
            _health = value;
            RefreshHealthBar();
        }
    }
    public float Fraction
    {
        get {
            if (_maxHealth == 0) _maxHealth = GameSettings.GetSetting(GameSettings.Type.PLAYER_HEALTH).Value;
            return _health / _maxHealth;
        }
    }

    int OwnCombo
    {
        get
        {
            if (_comboCounters == null) _comboCounters = new int[2];
            return _comboCounters[PlayerNum];
        }
        set
        {
            if (_comboCounters == null) _comboCounters = new int[2];
            _comboCounters[PlayerNum] = value;
        }
    }
    int OtherPlayerCombo
    {
        get
        {
            if (_comboCounters == null) _comboCounters = new int[2];
            return _comboCounters[(PlayerNum + 1) % 2];
        }
        set
        {
            if (_comboCounters == null) _comboCounters = new int[2];
            _comboCounters[(PlayerNum + 1) % 2] = value;
        }
    }
    int PlayerNum
    {
        get { return _controller.PlayerNum; }
    }

    //Initialisation

    /// <summary>
    /// Initialise values, add listeners
    /// </summary>
    private void Start()
    {
        if (PlayerHealths == null) PlayerHealths = new PlayerHealth[2];
        PlayerHealths[PlayerNum] = this;

        _maxHealth = GameSettings.GetSetting(GameSettings.Type.PLAYER_HEALTH).Value;
        Health = _maxHealth;
        _healthBar = GameSceneControl.GetHealthBar(PlayerNum);

        _lastHitTime = Time.time;
        _comboCounters = new int[2];

        GameSceneControl.GameEnd.AddListener(SubmitStreakScore);
        GameSceneControl.GameEnd.AddListener(SubmitComboScore);
    }
    /// <summary>
    /// Remove listeners
    /// </summary>
    private void OnDestroy()
    {
        GameSceneControl.GameEnd.RemoveListener(SubmitStreakScore);
        GameSceneControl.GameEnd.RemoveListener(SubmitComboScore);
    }

    //Update

    /// <summary>
    /// FixedUpdate checks to see if dodge time voice line needs triggering
    /// </summary>
    private void FixedUpdate()
    {
        if(Time.time - _lastHitTime > _dodgeLineTriggerTime)
        {
            if(!_lineTriggered)
            {
                _lineTriggered = true;
                if(PlayerNum == 0)
                {
                    Audio.VoicePlayer.PlayTrack(Audio.VoiceType.BLUE_NOT_HIT);
                }
                else
                {
                    Audio.VoicePlayer.PlayTrack(Audio.VoiceType.RED_NOT_HIT);
                }
            }
        }
        else
        {
            if(_lineTriggered)
            {
                _lineTriggered = false;
            }
        }
    }


    //Set methods

    /// <summary>
    /// Set the health to state.health
    /// </summary>
    public void SetHealthToStateHealth()
    {
        Health = _controller.state.Health;
    }


    //Damage methods

    /// <summary>
    /// Take damage and trigger death effect if below zero health
    /// </summary>
    public void Damage(float damage)
    {
        _health -= damage;

        if(_health <= 0)
        {
            _health = 0;
            _controller.PlayerDeath();
        }

        RefreshHealthBar();
        if (BoltNetwork.IsRunning && _controller.entity.isOwner) _controller.state.Health = _health;

    }
    /// <summary>
    /// Updates the health bar with the current fraction of health
    /// </summary>
    public void RefreshHealthBar()
    {
        if(_healthBar != null)
        {
            _healthBar.SetNewHealth(Fraction);
        }
    }

    //Collision event

    /// <summary>
    ///Check for collisions with projectiles (don't check if online and not owner)
    /// </summary>
    private void OnCollisionEnter(Collision collision)
    {
        if(!BoltNetwork.IsRunning || (BoltNetwork.IsRunning && BoltNetwork.IsServer))
        {
            if (collision.gameObject.tag == "Projectile")
            {
                float speed, scale, damage;
                speed = collision.gameObject.GetComponent<PlanetControl>().Speed;
                scale = collision.transform.localScale.x;
                damage = speed * Mathf.Pow(scale, _sizeDamagePowerScaling);
                
                PlanetControl pc = collision.gameObject.GetComponent<PlanetControl>();

                PlanetHit(damage, pc);
            }
        }
    }

    /// <summary>
    /// Handle all on hit effects
    /// </summary>
    private void PlanetHit(float damage, PlanetControl planetControl)
    {
        //Deal damage + shake camera
        Damage(damage);
        CamShake(damage);

        //Update stats
        SubmitStreakScore();
        SubmitComboScore();

        OtherPlayerCombo += 1;

        //Check health flags before playing voice line to ensure health lines get priority
        UpdateHealthFlags();

        //Trigger voice line and update stats according to ownership and damage
        if(PlayerNum == 0)
        {
            GameStats.SubmitNewScore(GameStats.FloatType.BLUE_BIGGEST_HIT_TAKEN, damage / _maxHealth);

            if (!planetControl.Owned)
            {
                VoicePlayer.PlayTrack(VoiceType.BLUE_HIT);
            }
            else if(planetControl.OwnedBy == 0)
            {
                VoicePlayer.PlayTrack(VoiceType.BLUE_HIT_SELF);
                GameStats.AddToScore(GameStats.IntType.BLUE_OWN_GOALS, 1);
                GameStats.AddToScore(GameStats.FloatType.BLUE_SELF_DAMAGE, damage / _maxHealth);
            }
            else if(damage > _bigHitThreshold)
            {
                VoicePlayer.PlayTrack(VoiceType.BLUE_HIT_BIG);
                GameStats.SubmitNewScore(GameStats.FloatType.RED_BIGGEST_HIT, damage / _maxHealth);
            }
            else
            {
                VoicePlayer.PlayTrack(VoiceType.BLUE_HIT_BY_RED);
                GameStats.SubmitNewScore(GameStats.FloatType.RED_BIGGEST_HIT, damage / _maxHealth);
            }
        }
        else
        {
            GameStats.SubmitNewScore(GameStats.FloatType.RED_BIGGEST_HIT_TAKEN, damage / _maxHealth);

            if (!planetControl.Owned)
            {
                VoicePlayer.PlayTrack(VoiceType.RED_HIT);
            }
            else if (planetControl.OwnedBy == 1)
            {
                VoicePlayer.PlayTrack(VoiceType.RED_HIT_SELF);
                GameStats.AddToScore(GameStats.IntType.RED_OWN_GOALS, 1);
                GameStats.AddToScore(GameStats.FloatType.RED_SELF_DAMAGE, damage / _maxHealth);
            }
            else if (damage > _bigHitThreshold)
            {
                VoicePlayer.PlayTrack(VoiceType.RED_HIT_BIG);
                GameStats.SubmitNewScore(GameStats.FloatType.BLUE_BIGGEST_HIT, damage / _maxHealth);
            }
            else
            {
                VoicePlayer.PlayTrack(VoiceType.RED_HIT_BY_BLUE);
                GameStats.SubmitNewScore(GameStats.FloatType.BLUE_BIGGEST_HIT, damage / _maxHealth);
            }
        }

        //Send event to other clients
        if(BoltNetwork.IsRunning && BoltNetwork.IsServer)
        {
            PlayerHit evnt = PlayerHit.Create();

            evnt.Player = PlayerNum;
            evnt.Damage = damage;
            evnt.PlanetId = planetControl.state.Id;
            evnt.NewHealth = _health;

            evnt.Send();
        }
        
    }
    /// <summary>
    /// Call flag triggers in gamescenecontrol
    /// </summary>
    private void UpdateHealthFlags()
    {
        if(PlayerNum == 0)
        {
            if (Fraction <= 0f) GameSceneControl.TriggerRedWin();
            if (Fraction < 0.1f) GameSceneControl.TriggerBlueHealth10();
            if (Fraction < 0.5f) GameSceneControl.TriggerBlueHealth50();
        }
        else
        {
            if (Fraction <= 0f) GameSceneControl.TriggerBlueWin();
            if (Fraction < 0.1f) GameSceneControl.TriggerRedHealth10();
            if (Fraction < 0.5f) GameSceneControl.TriggerRedHealth50();
        }
    }
    /// <summary>
    /// Shake the camera according to damage dealt
    /// </summary>
    private void CamShake(float damage)
    {
        Juce.singleton.CameraShake(damage / 100, 100f, 0.5f);
    }

    /// <summary>
    /// Listen for player hit events and call PlanetHit if this planet
    /// </summary>
    public override void OnEvent(PlayerHit evnt)
    {
        if(!BoltNetwork.IsServer && evnt.Player == _controller.PlayerNum)
        {
            _health = evnt.NewHealth;
            Damage(0f);                 //Call damage with parameter of 0f to trigger death effects etc
            CamShake(evnt.Damage);
        }
    }


    //Game stats methods
    
    private void SubmitStreakScore()
    {
        if(PlayerNum == 0)
        {
            GameStats.SubmitNewScore(GameStats.FloatType.BLUE_LONGEST_STREAK, Time.time - _lastHitTime);
        }
        else
        {
            GameStats.SubmitNewScore(GameStats.FloatType.RED_LONGEST_STREAK, Time.time - _lastHitTime);
        }
        _lastHitTime = Time.time;
    }
    private void SubmitComboScore()
    {
        if (PlayerNum == 0)
        {
            GameStats.SubmitNewScore(GameStats.IntType.BLUE_LONGEST_COMBO, OwnCombo);
        }
        else
        {
            GameStats.SubmitNewScore(GameStats.IntType.RED_LONGEST_COMBO, OwnCombo);
        }
        OwnCombo = 0;
    }
    
}
