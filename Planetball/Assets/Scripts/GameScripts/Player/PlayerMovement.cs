﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : Bolt.EntityBehaviour<IPlayer>
{
    //References
    [SerializeField] PlayerControl _controller;
    [SerializeField] Rigidbody _rigidbody;
    
    //Variables
    [SerializeField] float SpinSpeed = 1.5f;
    float Speed = 5;

    private KeyCode _left;
    private KeyCode _right;
    private KeyCode _up;
    private KeyCode _down;

    //Initialisation
    
    /// <summary>
    /// Set controls according to player number (p1 only when online)
    /// Set speed from options
    /// </summary>
    private void Start()
    {
        if(_controller.PlayerNum == 0 || BoltNetwork.IsRunning)
        {
            _left = Controls.GetKey("p1left");
            _right = Controls.GetKey("p1right");
            _up = Controls.GetKey("p1up");
            _down = Controls.GetKey("p1down");
        }
        else if(_controller.PlayerNum == 1)
        {
            _left = Controls.GetKey("p2left");
            _right = Controls.GetKey("p2right");
            _up = Controls.GetKey("p2up");
            _down = Controls.GetKey("p2down");
        }

        Speed = GameSettings.GetSetting(GameSettings.Type.PLAYER_SPEED).Value;
    }

    /// <summary>
    /// Updates: use FixedUpdate offline, and SimulateOwner online
    /// </summary>
    private void FixedUpdate () {
        
        if(!BoltNetwork.IsRunning)
        {
            _rigidbody.angularVelocity = new Vector3(-SpinSpeed, SpinSpeed, 0f);
            SetVelocity();
        }
	}
    public override void SimulateOwner()
    {
        _rigidbody.angularVelocity = new Vector3(-SpinSpeed, SpinSpeed, 0f);
        SetVelocity();
    }

    /// <summary>
    /// Set the velocity according to user input; called by update methods
    /// </summary>
    private void SetVelocity()
    {
        Vector3 v = Vector3.zero;

        if(Input.GetKey(_left))
        {
            v += Vector3.left;
        }
        if (Input.GetKey(_right))
        {
            v += Vector3.right;
        }
        if (Input.GetKey(_up))
        {
            v += Vector3.up;
        }
        if (Input.GetKey(_down))
        {
            v += Vector3.down;
        }

        _rigidbody.velocity = v.normalized * Speed;
    }
}
