﻿using System.Collections.Generic;
using UnityEngine;

public class AttractionController : Bolt.GlobalEventListener {

    //References
    [Header("References")]
    [SerializeField] PlayerControl _controller;
    [SerializeField] PinTransform _pin;
    [SerializeField] AttractionParticlesController _particles;

    public AttractionParticlesController Particles
    {
        get { return _particles; }
    }

    //Properties
    [Header("Targeting Properties")]
    [SerializeField] float _maxRange = 10;
    [SerializeField] float _maxPlanetSpeed = 20f;
    [Header("Pulling Properties")]
    [SerializeField] float _angleCorrectionRate = 30f;
    [SerializeField] float _planetAcceleration = 5f;
    [Tooltip("High values will result in larger planets accelerating more slowly")]
    [SerializeField] float _accelerationInversePower = 0.5f;

    //Private variables
    PlanetControl _tracking = null;
    System.Guid _trackedId;
    bool _buttonDown = false;
    bool _buttonUp = false;
    bool _pulling = false;
    KeyCode key;

    //Set key according to player num
    private void Start()
    {
        if (BoltNetwork.IsRunning || _controller.PlayerNum == 0)
        {
            key = Controls.GetKey("p1grav");
        }
        else
        {
            key = Controls.GetKey("p2grav");
        }

        _maxPlanetSpeed = GameSettings.GetSetting(GameSettings.Type.PLAYER_GRAV_TOPSPEED).Value;
        _planetAcceleration = GameSettings.GetSetting(GameSettings.Type.PLAYER_GRAV_ACCEL).Value;
    }
    //Use update method to check for input
    private void Update()
    {
        if (!BoltNetwork.IsRunning || _controller.entity.isOwner)
        {
            if (Input.GetKeyDown(key))
            {
                _buttonDown = true;
            }
            if (Input.GetKeyUp(key))
            {
                _buttonUp = true;
            }
        }
    }

    private void FixedUpdate()
    {
        Targeting();
        if (_pulling && (!BoltNetwork.IsRunning || BoltNetwork.IsServer))
        {
            PullControl();
        }
    }

    /// <summary>
    /// Responsible for target finding and setting pull on and off
    /// </summary>
    private void Targeting()
    {
        if (_pulling)
        {
            if (_tracking == null || _buttonUp || _tracking.Distance(transform.position) > _maxRange || _tracking.Speed > _maxPlanetSpeed)
            {
                ReleaseTracked();
            }
        }
        else
        {
            PlanetControl target = FindClosestTarget();
            if (target == null)
            {
                StopTracking();
            }
            else
            {
                SetTracking(target);
            }

            if (_buttonDown && target != null)
            {
                GrabTracked();
            }
        }

        _buttonDown = false;
        _buttonUp = false;
    }
    /// <summary>
    /// Returns the closest planet that can be grabbed, or null if none available
    /// </summary>
    private PlanetControl FindClosestTarget()
    {
        List<PlanetControl> sortedList = PlanetControl.SortByDistance(transform.position);

        for (int i = 0; i < sortedList.Count; ++i)
        {
            PlanetControl gt = sortedList[i];

            if (!gt.IsGrabbed && gt.Distance(transform.position) < _maxRange && gt.Speed < _maxPlanetSpeed)
            {
                return gt;
            }
        }

        return null;
    }
    /// <summary>
    /// Begin tracking the target
    /// </summary>
    private void SetTracking(PlanetControl gt)
    {
        _tracking = gt;
        _pin.LocationPin = gt.transform;
        _pin.Active = true;
        _particles.SetPassive();

        if(BoltNetwork.IsRunning)
        {
            _trackedId = _tracking.state.Id;
        }
    }
    /// <summary>
    /// Stop tracking any target
    /// </summary>
    private void StopTracking()
    {
        _tracking = null;
        _pin.Active = false;
        _particles.SetDisabled();
    }
    /// <summary>
    /// Begin pulling the target
    /// </summary>
    private void GrabTracked()
    {
        if (!BoltNetwork.IsRunning)
        {
            Grab();
            _tracking.Movement.LoopedScreen.AddListener(ReleaseTracked);
            _tracking.CollisionComponent.PlanetPlanetCollision.AddListener(ReleaseTracked);
        }
        else if (_controller.entity.isOwner)
        {
            PlanetGrabbed evnt = PlanetGrabbed.Create();
            evnt.PlayerId = _controller.state.PlayerNum;
            evnt.PlanetId = _tracking.state.Id;
            evnt.Send();
        }
    }
    public override void OnEvent(PlanetGrabbed evnt)
    {
        if (evnt.PlayerId == _controller.state.PlayerNum)
        {
            if (evnt.PlanetId != _trackedId)
            {
                SetTracking(PlanetControl.FindById(evnt.PlanetId));
            }

            Grab();
        }
    }
    private void Grab()
    {
        _tracking.GrabPlanet(_controller.PlayerNum);
        _pulling = true;
        _particles.SetPulling();

        _initialAngle = _tracking.Angle(transform.position);
        //_initialRadius = _tracking.Distance(transform.position);
        //_initialSpeed = _tracking.Speed;
        _angle = _initialAngle;
    }
    /// <summary>
    /// Stop pulling the target
    /// </summary>
    private void ReleaseTracked()
    {
        if (!BoltNetwork.IsRunning)
        {
            if(_tracking != null)
            {
                _tracking.Movement.LoopedScreen.RemoveListener(ReleaseTracked);
                _tracking.CollisionComponent.PlanetPlanetCollision.RemoveListener(ReleaseTracked);
            }
            Release();
        }
        else if (_controller.entity.isOwner)
        {
            PlanetReleased evnt = PlanetReleased.Create();
            evnt.PlayerId = _controller.state.PlayerNum;
            evnt.Send();
        }
    }
    public override void OnEvent(PlanetReleased evnt)
    {
        if (_tracking != null)
        {
            if (evnt.PlayerId == _controller.state.PlayerNum)
            {
                Release();
            }
        }
    }
    public override void OnEvent(PlanetLooped evnt)
    {
        if (_tracking != null)
        {
            if (evnt.PlanetId == _trackedId && _pulling)
            {
                Release();
            }
        }
    }
    public override void OnEvent(PlanetBump evnt)
    {
        if (_tracking != null)
        {
            if (evnt.PlanetId == _trackedId && _pulling)
            {
                Release();
            }
        }
    }
    public override void OnEvent(PlanetDestruction evnt)
    {
        if (evnt.Id == _trackedId && _pulling)
        {
            _pulling = false;
            _particles.SetPassive();

            StopTracking();
        }
    }
    private void Release()
    {
        if(_tracking != null)
        {
            _tracking.ReleasePlanet();
        }
        _pulling = false;
        _particles.SetPassive();
    }


    private float _initialAngle;
    //private float _initialSpeed;
    //private float _initialRadius;

    private float _angle;

    /// <summary>
    /// Responsible for applying physics to target
    /// </summary>
    private void PullControl()
    {
        AdjustAngle();
        SetVelocity();
    }
    /// <summary>
    /// Responsible for adjusting the angle
    /// </summary>
    private void AdjustAngle()
    {
        if (_angle != 90f)
        {
            if (Mathf.Abs(_angle) < 90f)     //If planet travelling towards player, let angle change naturally to 90 deg
            {
                float newAngle = _tracking.Angle(transform.position);
                if (Mathf.Abs(newAngle) > 90f)
                {
                    _angle = newAngle;
                }
            }
            else
            {
                _angle -= _angleCorrectionRate * Time.fixedDeltaTime * Mathf.Sign(_angle);
                if (Mathf.Abs(_angle) < 90f)
                {
                    _angle = 90f * Mathf.Sign(_angle);
                }
            }
        }
    }
    /// <summary>
    /// Responsible for setting the new velocity including acceleration and redirection
    /// </summary>
    private void SetVelocity()
    {
        //Increase speed
        float speed = _tracking.Speed;
        if (Mathf.Abs(_angle) <= 90f && speed <= _maxPlanetSpeed)
        {
            speed += (_planetAcceleration / Mathf.Pow(_tracking.transform.localScale.x, _accelerationInversePower)) * Time.fixedDeltaTime;
        }

        //Set new velocity
        if (Mathf.Abs(_angle) < 90f)
        {
            _tracking.Velocity = _tracking.Velocity.normalized * speed;
        }
        else
        {
            float radius = _tracking.Distance(transform.position);

            float angleAdjustment;      //Slight adjustment to move in perfect circle
            if (_angle == -90f)
            {
                angleAdjustment = Mathf.Rad2Deg * Mathf.Asin(speed * Time.fixedDeltaTime / (2f * radius));
            }
            else if (_angle == 90f)
            {
                angleAdjustment = Mathf.Rad2Deg * -Mathf.Asin(speed * Time.fixedDeltaTime / (2f * radius));
            }
            else
            {
                angleAdjustment = 0f;
            }
            _tracking.Velocity = Quaternion.Euler(0f, 0f, _angle + angleAdjustment) * (transform.position - _tracking.transform.position).normalized * speed;
        }
    }

    //Disable/destroy particles gameobject
    public void DisableParticlesObject()
    {
        if(_particles != null)
        {
            _particles.gameObject.SetActive(false);
        }
    }
    public void DestroyParticlesObject()
    {
        if (_particles != null)
        {
            Destroy(_particles.gameObject);
        }
    }

}
