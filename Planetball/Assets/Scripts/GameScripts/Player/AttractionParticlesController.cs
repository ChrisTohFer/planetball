﻿using UnityEngine;

//Class controls the properties of the particlesystem at runtime

public class AttractionParticlesController : MonoBehaviour {

    [Header("References")]
    public ParticleSystem Particles;
    public Transform Target;

    [Header("Active Properties")]
    public float AParticleTime;
    public float AParticleRate;
    public Color AParticleColor;

    [Header("Passive Properties")]
    public float PParticleTime;
    public float PParticleRate;
    public Color PParticleColor;

    //Private
    private bool _active = false;
    private bool _pulling = false;

    //Private references to particle system components and values for distance to target
    private ParticleSystem.MainModule _main;
    private ParticleSystem.EmissionModule _emission;
    private float _oldDistance, _newDistance, _dz;

    //Private value for current speed depending on state
    private float _currentParticleTime;

    private void Start()
    {
        _main = Particles.main;
        _emission = Particles.emission;

        _oldDistance = _newDistance = (Target.position - transform.position).magnitude;
        _dz = 0f;

        transform.parent = null;
    }

    //Clear particles or update particlesystem properties as appropriate
    private void FixedUpdate()
    {
        SetDistance();

        if(!_active)
        {
            Particles.Stop();
            Particles.Clear();
        }
        else
        {
            if(!Particles.isPlaying)
            {
                Particles.Play();
            }

            ScaleParticles();
            _main.startSpeed = _newDistance / _currentParticleTime;
            //CullParticles();
        }
    }

    /// <summary>
    /// Returns true if in the pulling state
    /// </summary>
    public bool Pulling()
    {
        return (_pulling && _active);
    }
    /// <summary>
    /// Returns true if in the passive state
    /// </summary>
    public bool Passive()
    {
        return (!_pulling && _active);
    }

    /// <summary>
    /// Sets particle system into the disabled state
    /// </summary>
    public void SetDisabled()
    {
        _active = false;
        Particles.Clear();
        Particles.Stop();
    }
    /// <summary>
    /// Sets particle system into the pulling state (faster, bolder)
    /// </summary>
    public void SetPulling()
    {
        _active = true;
        _pulling = true;
        _main.startSpeed = _newDistance / AParticleTime;
        _main.startLifetime = AParticleTime;
        _main.startColor = AParticleColor;
        _emission.rateOverTime = AParticleRate;

        _currentParticleTime = AParticleTime;
    }
    /// <summary>
    /// Sets particle system into the passive state (slower, fainter)
    /// </summary>
    public void SetPassive()
    {
        _active = true;
        _pulling = false;
        _main.startSpeed = _newDistance / PParticleTime;
        _main.startLifetime = PParticleTime;
        _main.startColor = PParticleColor;
        _emission.rateOverTime = PParticleRate;

        _currentParticleTime = PParticleTime;
    }
    
    /// <summary>
    /// Sets the _oldDistance, _newDistance and _dz variables
    /// </summary>
    /// <returns></returns>
    private void SetDistance()
    {
        _oldDistance = _newDistance;
        _newDistance = (Target.position - transform.position).magnitude;

        _dz = _newDistance - _oldDistance;
    }
    /// <summary>
    /// Look through particles and delete any that have passed the target due to movement
    /// </summary>
    private void CullParticles()
    {
        ParticleSystem.Particle[] particleList = new ParticleSystem.Particle[Particles.particleCount];
        Particles.GetParticles(particleList);

        for(int i = 0; i < particleList.Length; ++i)
        {
            if(particleList[i].position.magnitude > _newDistance)
            {
                particleList[i].remainingLifetime = -1f; 
            }
        }

        Particles.SetParticles(particleList, particleList.Length);
    }
    /// <summary>
    /// Scale distances of currently active particles according to movement of transforms
    /// </summary>
    private void ScaleParticles()
    {
        if(Particles.particleCount > 0)
        {
            ParticleSystem.Particle[] particleList = new ParticleSystem.Particle[Particles.particleCount];
            Particles.GetParticles(particleList);

            float scaleFactor = 1.0f + _dz / _oldDistance;

            for (int i = 0; i < particleList.Length; ++i)
            {
                particleList[i].position = new Vector3(particleList[i].position.x, particleList[i].position.y, scaleFactor * particleList[i].position.z);
                particleList[i].velocity = particleList[i].velocity * scaleFactor;
            }

            Particles.SetParticles(particleList, particleList.Length);
        }
    }
    
}
