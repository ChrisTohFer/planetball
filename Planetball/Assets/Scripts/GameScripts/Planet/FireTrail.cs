﻿using System.Collections;
using UnityEngine;

//Class activates a particlesystem (intended for fire trails) when rigidbody reaches certain velocity

public class FireTrail : Bolt.EntityBehaviour<IPlanet> {

    [Header("References")]
    [SerializeField] Rigidbody _rigidbody;
    [SerializeField] ParticleSystem _particle;
    [SerializeField] AudioSource _screamTeam;

    [Header("Properties")]
    [SerializeField] float _speedThreshold = 10f;
    [SerializeField] float _maxSpeed = 20f;
    [SerializeField] float _volumeChangeRate = 0.5f;
    [SerializeField] float _pitchMin = 0.8f;
    [SerializeField] float _pitchMax = 1.1f;

    private bool _playing = false;
    private float _targetVolume;
    private float _rateOverDistance;
    private ParticleSystem.EmissionModule _emission;

    //Particles are emitted over time to avoid glitchy trails, but rate is calculated according to speed
    private void Start()
    {
        _emission = _particle.emission;
        _rateOverDistance = _emission.rateOverDistance.constant;
        _emission.rateOverDistance = 0f;
    }
    //Stop on disable
    private void OnDisable()
    {
        Stop(true);
    }

    //Check speed variable, update screaming volume, start/stop particle effect
    private void FixedUpdate()
    {
        float speed;
        if (BoltNetwork.IsRunning)
        {
            speed = state.Velocity.magnitude;
        }
        else
        {
            speed = _rigidbody.velocity.magnitude;
        }

        //Scale volume between threshold and maxspeed
        _targetVolume = Options.O.ScreamVolume * (Mathf.Clamp(speed, _speedThreshold, _maxSpeed) - _speedThreshold) / (_maxSpeed - _speedThreshold);
        _screamTeam.pitch = _pitchMin + (_pitchMax - _pitchMin) * (Mathf.Clamp(speed, _speedThreshold, _maxSpeed) - _speedThreshold) / (_maxSpeed - _speedThreshold);

        if (speed > _speedThreshold)
        {
            if(!_playing)
            {
                StartPlaying();
            }
            _emission.rateOverTime = speed * _rateOverDistance;
        }
        else
        {
            if(_playing)
            {
                Stop(false);
            }
        }
    }

    //Methods to start and stop
    private void StartPlaying()
    {
        _particle.Play();
        StopAllCoroutines();
        StartCoroutine(VolumeControl());
        _playing = true;
    }
    private void Stop(bool immediate)
    {
        _particle.Stop();

        if (immediate)
        {
            StopAllCoroutines();
            _screamTeam.volume = 0f;
            _screamTeam.Stop();
        }

        _playing = false;
    }
    
    //Coroutine runs during animation to control volume of the screaming
    private IEnumerator VolumeControl()
    {
        if(!_screamTeam.isPlaying)
        {
            _screamTeam.Play();
        }

        while(_screamTeam.isPlaying)
        {
            if(_targetVolume != _screamTeam.volume)
            {
                float signOfChange = Mathf.Sign(_targetVolume - _screamTeam.volume);
                _screamTeam.volume += signOfChange * _volumeChangeRate * Options.O.ScreamVolume * Time.fixedDeltaTime;

                if (Mathf.Sign(_targetVolume - _screamTeam.volume) != signOfChange)
                {
                    _screamTeam.volume = _targetVolume;
                }
            }
            
            if(_targetVolume == 0f && _screamTeam.volume == 0f)
            {
                break;
            }

            yield return new WaitForFixedUpdate();
        }

        _screamTeam.Stop();
    }

}
