﻿using UnityEngine;
using UnityEngine.Events;

//Class handles collisions of projectile planets with player and with other projectile planets
public class PlanetCollision : Bolt.GlobalEventListener {

    //References
    [Header("Component references")]
    [SerializeField] PlanetControl _controller;
    [SerializeField] RandomSound _collisionSounds;
    [SerializeField] Rigidbody _rigidbody;

    //Variables
    [Header("Properties")]
    [SerializeField] float _relativeSpeedForExplosion = 15f;
    [SerializeField] float _planetTrampleThreshold = 1.5f;

    //
    public UnityEvent PlanetPlanetCollision;
    
    //Handle collisions with planets
    private void OnCollisionEnter(Collision collision)
    {
        if(!BoltNetwork.IsRunning)
        {
            //When offline, handle planet destruction here
            if (collision.transform.tag == "Player")
            {
                DestroyPlanet();
            }
            else if (collision.transform.tag == "Projectile")
            {
                PlanetPlanetCollision.Invoke();

                //Destroy or play sound effect based on size/speed
                if (collision.relativeVelocity.magnitude > _relativeSpeedForExplosion && transform.localScale.x < collision.transform.localScale.x * _planetTrampleThreshold)
                {
                    DestroyPlanet();
                }
                else
                {
                    _collisionSounds.PlayRandomSound();
                }

            }
        }
        else
        {
            //Server handles planet planet collisions when online
            if (BoltNetwork.IsServer)
            {
                if (collision.transform.tag == "Projectile")
                {

                    if (collision.relativeVelocity.magnitude > _relativeSpeedForExplosion && transform.localScale.x < collision.transform.localScale.x * _planetTrampleThreshold)
                    {
                        DestroyPlanet();
                    }
                    else
                    {
                        PlanetBump evnt = PlanetBump.Create();
                        evnt.PlanetId = _controller.state.Id;
                        evnt.Send();
                    }

                }
            }
        }
    }
    
    public override void OnEvent(PlanetBump evnt)
    {
        if (evnt.PlanetId == _controller.state.Id)
        {
            _collisionSounds.PlayRandomSound();
        }
    }

    public override void OnEvent(PlayerHit evnt)
    {
        if (_controller.state.Id == evnt.PlanetId)
        {
            DestroyPlanet();
        }
    }

    public void DestroyPlanet()
    {
        if (!BoltNetwork.IsRunning)
        {
            GameSceneControl.CreateExplosion(transform.position, _rigidbody.velocity, transform.localScale.x);
            Destroy(gameObject);
        }
        else if (BoltNetwork.IsServer)
        {
            PlanetDestruction evnt = PlanetDestruction.Create();
            evnt.Id = _controller.state.Id;
            evnt.Position = transform.position;
            evnt.Scale = transform.localScale.x;
            evnt.Velocity = _controller.Velocity;
            evnt.Send();

            BoltNetwork.Destroy(gameObject);
        }
    }

}
