﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

//Central class for planets with references to components

public class PlanetControl : Bolt.EntityBehaviour<IPlanet> {

    //References
    [Header("Component references")]
    [SerializeField] Rigidbody _rigidbody;
    [SerializeField] Transform _meshTransform;
    [SerializeField] PlanetMovement _movement;
    [SerializeField] PlanetCollision _collision;

    public PlanetMovement Movement
    {
        get { return _movement; }
    }
    public PlanetCollision CollisionComponent
    {
        get { return _collision; }
    }

    //Static list of planets when components need to be accessed, or planets need to be counted
    static List<PlanetControl> _planetList = new List<PlanetControl>();
    public static List<PlanetControl> PlanetList
    {
        get { return _planetList; }
    }
    //Find a planet in the list by its id
    public static PlanetControl FindById(System.Guid id)
    {
        Debug.Log(PlanetList.Count);
        for (int i = 0; i < PlanetList.Count; ++i)
        {
            if (PlanetList[i].state.Id == id)
            {
                return PlanetList[i];
            }
        }

        return null;
    }

    //Update list on creation and destruction
    private void Start()
    {
        _planetList.Add(this);
        if (BoltNetwork.IsServer)
        {
            state.Scale = transform.localScale.x;
            state.Id = System.Guid.NewGuid();
        }
    }
    public override void Detached()
    {
        _planetList.Remove(this);
    }
    private void OnDestroy()
    {
        if(!BoltNetwork.IsRunning)
        {
            _planetList.Remove(this);
        }
    }

    //Setup callbacks and synced properties
    public override void Attached()
    {
        state.SetTransforms(state.PlanetTransform, transform, _meshTransform);
        state.AddCallback("Scale", OnChangeScale);
        state.AddCallback("Velocity", OnChangeVelocity);

        if (BoltNetwork.IsServer)
        {
            
        }
        else
        {
            //Set rigidbody to kinematic if not server (server handles physics)
            GetComponentInChildren<Collider>().enabled = false;
            _rigidbody.isKinematic = true;
            _rigidbody.interpolation = RigidbodyInterpolation.None;
        }
    }

    //Callbacks

    //Set local scale
    public void OnChangeScale()
    {
        if(!BoltNetwork.IsServer)
        {
            transform.localScale = new Vector3(state.Scale, state.Scale, state.Scale);
        }
    }
    //Set local velocity variable
    public void OnChangeVelocity()
    {
        _velocity = state.Velocity;
    }

    //If offline update velocity in fixedupdate
    private void FixedUpdate()
    {
        if(!BoltNetwork.IsRunning)
        {
            _velocity = _rigidbody.velocity;
        }
        else if(BoltNetwork.IsServer)
        {
            state.Velocity = _rigidbody.velocity;
        }

        if (!_grabbed)
        {
            if (_ownedCounter >= 0f) { _ownedCounter -= Time.fixedDeltaTime; }
        }
    }


    //Gravtarget stuff

    //Variables
    Vector3 _velocity = Vector3.zero;
    bool _grabbed = false;

    [SerializeField] float _ownedLength = 5f;
    int _ownedBy = 0;
    float _ownedCounter = -1f;
    
    [HideInInspector]
    public UnityEvent Escaped;

    //Property get and set
    public float Speed
    {
        get { return _velocity.magnitude; }
    }
    public Vector3 Velocity
    {
        get { return _velocity; }
        set {
            if(BoltNetwork.IsRunning)
            {
                _rigidbody.velocity = value;
                _velocity = value;
            }
            else
            {
                _velocity = value;
                _rigidbody.velocity = value;
            }
        }
    }
    public bool IsGrabbed
    {
        get { return _grabbed; }
    }
    public float Distance(Vector3 point)
    {
        return (point - transform.position).magnitude;
    }
    public float Angle(Vector3 point)
    {
        return Vector3.SignedAngle((point - transform.position), Velocity, Vector3.forward);
    }
    public bool Owned
    {
        get { return _ownedCounter > 0f; }
    }
    public int OwnedBy
    {
        get { return _ownedBy; }
    }

    //Attraction methods
    public void GrabPlanet(int player)
    {
        _ownedBy = player;
        _ownedCounter = _ownedLength;
        _grabbed = true;
    }
    public void ReleasePlanet()
    {
        _ownedCounter = _ownedLength;
        _grabbed = false;
    }
    
    /// <summary>
    /// Returns the planet list sorted in order of distance to point
    /// </summary>
    public static List<PlanetControl> SortByDistance(Vector3 point)
    {
        List<PlanetControl> Sorted = new List<PlanetControl>(PlanetList);

        //Calculate distances
        float[] distances = new float[Sorted.Count];
        for (int i = 0; i < Sorted.Count; ++i)
        {
            distances[i] = (Sorted[i].transform.position - point).magnitude;
        }

        //Sort by distance
        bool condition = true;
        while (condition)
        {
            condition = false;
            for (int i = 0; i < Sorted.Count - 1; ++i)
            {
                if (distances[i] > distances[i + 1])
                {
                    float tempDist = distances[i + 1];
                    distances[i + 1] = distances[i];
                    distances[i] = tempDist;

                    PlanetControl tempGT = Sorted[i + 1];
                    Sorted[i + 1] = Sorted[i];
                    Sorted[i] = tempGT;

                    condition = true;
                }
            }
        }

        return Sorted;
    }

}
