﻿using System.Collections;
using UnityEngine;

public class PlanetSpawner : Bolt.GlobalEventListener
{

    [Header("Asset references")]
    [SerializeField] GameObject _planetPrefab;

    [Header("Scaling Properties")]
    [SerializeField] float _maxSizeStart = 1f;
    [SerializeField] float _maxSizeIncrement = 0.5f;
    [SerializeField] int _maxScaleLevel = 5;

    [Header("Spawning properties")]
    [SerializeField] int _maxPlanets = 5;
    [SerializeField] float _cooldown = 5f;
    [SerializeField] float _initialWait = 3f;

    //Keep track of how many spawned
    float _spawned;

    private void Awake()
    {
        _spawned = 0;
    }
    private void Start()
    {
        _cooldown = GameSettings.GetSetting(GameSettings.Type.PLANET_COOLDOWN).Value;
        _maxPlanets = GameSettings.GetSetting(GameSettings.Type.PLANET_MAXCOUNT).ValueInt;
        _maxSizeStart = GameSettings.GetSetting(GameSettings.Type.PLANET_MINSIZE).Value;
        _maxSizeIncrement = (GameSettings.GetSetting(GameSettings.Type.PLANET_MAXSIZE).Value - _maxSizeStart) / _maxScaleLevel;

        if (!BoltNetwork.IsRunning || BoltNetwork.IsServer)
            StartCoroutine(SpawnEveryPeriod(_initialWait, _cooldown));
    }

    //Spawns a planet every period seconds, and will attempt to spawn every fixedupdate if unable
    private IEnumerator SpawnEveryPeriod(float delay, float period)
    {
        yield return new WaitForSeconds(delay);

        while (true)
        {
            while (!SpawnPlanet())
            {
                yield return new WaitForFixedUpdate();  //If planet was not spawned try again next update
            }
            yield return new WaitForSeconds(period);
        }
    }

    /// <summary>
    /// Creates a new planet if cap is not reached
    /// </summary>
    public bool SpawnPlanet()
    {
        
        if (PlanetControl.PlanetList.Count < _maxPlanets)
        {
            GameObject go;
            if (BoltNetwork.IsRunning)
            {
                //Planet spawned off screen, will be moved to new position in OnEnable() event
                go = BoltNetwork.Instantiate(BoltPrefabs.Planet, new Vector3(-30f, 0f, 0f), Quaternion.identity);
            }
            else
            {
                go = Instantiate(_planetPrefab, new Vector3(-30f, 0f, 0f), Quaternion.identity);
            }

            //Randomise the scale of the planet based on size scaling
            float scale = _maxSizeStart + Mathf.Min(_spawned, _maxScaleLevel) * _maxSizeIncrement * Random.value;
            go.transform.localScale = new Vector3(scale, scale, scale);
            go.GetComponent<Rigidbody>().mass = scale * scale;

            ++_spawned;

            return true;

        }
        else
        {
            return false;   //Planet wasn't spawned
        }

    }
}
