﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Field : MonoBehaviour {
    
    public Rect Rectangle { get; private set; }

    private BoxCollider bc;

    void Awake()
    {
        bc = GetComponent<BoxCollider>();

        Rectangle = new Rect(bc.center.x - 0.5f * bc.size.x, bc.center.y - 0.5f * bc.size.y, bc.size.x, bc.size.y);
    }

    /// <summary>
    /// Function is analogous to Mathf.Repeat but for a rectangular area
    /// </summary>
    public Vector3 CyclePoint(Vector3 point)
    {
        float x = Mathf.Repeat(point.x - Rectangle.x, Rectangle.size.x) + Rectangle.x;
        float y = Mathf.Repeat(point.y - Rectangle.y, Rectangle.size.y) + Rectangle.y;

        return new Vector3(x, y, point.z);
    }
    /// <summary>
    /// Returns closest point on edge of field
    /// </summary>
    public Vector3 ClampPoint(Vector3 point)
    {
        float x = Mathf.Clamp(point.x, Rectangle.xMin, Rectangle.xMax);
        float y = Mathf.Clamp(point.y, Rectangle.yMin, Rectangle.yMax);

        return new Vector3(x, y, point.z);
    }
    public Vector3 FlipPoint(Vector3 point)
    {
        float x, y;
        //Flip in the dimension that is the greater relative distance from the center
        if(point.x / Rectangle.size.x > point.y / Rectangle.size.y)
        {
            x = -point.x;
            y = point.y;
        }
        else
        {
            x = point.x;
            y = -point.y;
        }

        return new Vector3(x, y, point.z);
    }

    /// <summary>
    /// Returns a random point on the edge of the field
    /// </summary>
    public Vector3 FieldEdgeRandomPoint(out Vector3 inwards)
    {
        float width = Rectangle.size.x;
        float height = Rectangle.size.y;
        float point = Random.value * 2f * (width + height);

        if (point < width)
        {
            inwards = Vector3.down;
            return new Vector3(Rectangle.xMin + point, Rectangle.yMax, 0f);
        }
        else if (point < width + height)
        {
            inwards = Vector3.left;
            return new Vector3(Rectangle.xMax, Rectangle.yMax - (point - width), 0f);
        }
        else if (point < 2f * width + height)
        {
            inwards = Vector3.up;
            return new Vector3(Rectangle.xMax - (point - width - height), Rectangle.yMin, 0f);
        }
        else
        {
            inwards = Vector3.right;
            return new Vector3(Rectangle.xMin, Rectangle.yMin + (point - 2f * width - height));
        }
    }
}
