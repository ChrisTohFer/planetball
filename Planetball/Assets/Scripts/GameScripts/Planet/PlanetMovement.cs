﻿using UnityEngine;
using UnityEngine.Events;

public class PlanetMovement : Bolt.EntityBehaviour<IPlanet>
{
    //References
    [Header("References")]
    [SerializeField] Rigidbody _rigidbody;

    [Header("Properties")]
    [SerializeField] float _initialSpin = 1.5f;
    [SerializeField] float _respawnMaxAngle = 1f;
    [SerializeField] bool _loopOnBoundary = false;

    [Header("Events")]
    public UnityEvent LoopedScreen;

    //Initialise movement and location
    private void Start()
    {
        RandomSpin();
        RandomSpawn(GameSettings.GetSetting(GameSettings.Type.PLANET_SPAWNSPEED).Value);
    }
    /// <summary>
    /// Spin in a random direction
    /// </summary>
    private void RandomSpin()
    {
        _rigidbody.angularVelocity = _initialSpin * Random.onUnitSphere;
    }
    /// <summary>
    /// Spawn planet in a random part of the boundary with specified speed
    /// </summary>
    private void RandomSpawn(float speed)
    {
        Vector3 inwards;
        Vector3 point = GameSceneControl.PlanetField.FieldEdgeRandomPoint(out inwards);

        transform.position = point;

        float angle = 2f * (Random.value - 0.5f) * _respawnMaxAngle;
        Vector3 velocity = Quaternion.Euler(0f, 0f, angle) * inwards * speed;

        _rigidbody.velocity = velocity;
    }

    //Check screen looping during update
    private void FixedUpdate()
    {
        if(!BoltNetwork.IsRunning)
        {
            CheckScreenLoop();
        }
    }
    public override void SimulateOwner()
    {
        CheckScreenLoop();
    }
    private void CheckScreenLoop()
    {
        if (_loopOnBoundary && !GameSceneControl.TriggeredEnding)
        {
            Vector3 Initial = transform.position;
            transform.position = GameSceneControl.PlanetField.CyclePoint(transform.position);

            if ((Initial - transform.position).magnitude > 1f)
            {
                LoopedEvent();
            }
        }
    }

    //If screen looping is turned off, spawn in random location on playfield exit
    private void OnTriggerExit(Collider other)
    {
        if(!BoltNetwork.IsRunning || (BoltNetwork.IsRunning && entity.isOwner))
        {
            if (other.gameObject.tag == "PlayField")
            {
                if (!_loopOnBoundary)
                {
                    RandomSpawn(_rigidbody.velocity.magnitude);
                    LoopedEvent();
                }
            }
        }
    }

    /// <summary>
    /// Called when planet leaves the screen
    /// </summary>
    private void LoopedEvent()
    {
        if (BoltNetwork.IsRunning)
        {
            PlanetLooped evnt = PlanetLooped.Create();
            evnt.PlanetId = state.Id;
            evnt.Send();
        }
        else
        {
            LoopedScreen.Invoke();
        }
    }

}
