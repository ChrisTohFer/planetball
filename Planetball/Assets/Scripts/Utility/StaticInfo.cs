﻿
public static class StaticInfo {

    //Global variable to keep track of version when interfacing with other files and online functionality
    public static string Version = "0.2.0";

    //Keep track of how many times scenes have been opened
    public static int MenuScene = 0;
    public static int GameScene = 0;
    
}
