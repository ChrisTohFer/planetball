﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyPlayerList : MonoBehaviour {

    //References

    [SerializeField] PlayerBadge _blueBadge;
    [SerializeField] PlayerBadge _redBadge;
    [SerializeField] List<PlayerBadge> _spectatorBadges;


    //Initialisation

    /// <summary>
    /// On enable, trim list to maxiumum player count and listen for playerlist changes
    /// </summary>
    private void OnEnable()
    {
        //Set excess badges over maximum players to be inactive
        for(int i = 0; i < _spectatorBadges.Count; ++i)
        {
            _spectatorBadges[i].gameObject.SetActive(i < PlayerList.MaxPlayers);
        }

        PlayerList.OnListUpdated.AddListener(SetBadgeDetails);
        SetBadgeDetails();
    }
    /// <summary>
    /// On disable, stop listening for player list changes
    /// </summary>
    private void OnDisable()
    {
        PlayerList.OnListUpdated.RemoveListener(SetBadgeDetails);
    }


    //Methods

    /// <summary>
    /// Update the details of the blue, red, and spectator badges
    /// </summary>
    public void SetBadgeDetails()
    {
        PlayerList.Player player;
        
        player = PlayerList.BluePlayer;
        if(player != null)
        {
            _blueBadge.SetOccupied(player);
        }
        else
        {
            _blueBadge.SetEmpty();
        }

        player = PlayerList.RedPlayer;
        if (player != null)
        {
            _redBadge.SetOccupied(player);
        }
        else
        {
            _redBadge.SetEmpty();
        }

        int playerQueueNum = 0;
        for(int i = 0; i < PlayerList.PlayerCount; ++i)
        {
            player = PlayerList.Players[i];
            if(!player.IsBlue && !player.IsRed)
            {
                _spectatorBadges[playerQueueNum].SetOccupied(player);
                ++playerQueueNum;
            }
        }
        for(int i = playerQueueNum; i < _spectatorBadges.Count; ++i)
        {
            _spectatorBadges[i].SetEmpty();
        }
    }

}
