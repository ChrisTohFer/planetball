﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ServerPreviewController : MonoBehaviour {

    //References

    [SerializeField] TextMeshProUGUI _serverName;
    [SerializeField] TextMeshProUGUI _playerName;
    [SerializeField] TextMeshProUGUI _playerCount;
    [SerializeField] TextMeshProUGUI _passwordRequired;

    TMP_InputField _clientName;
    TMP_InputField _clientPassword;
    OnlineMenuController _menuController;

    ServerInfoToken _serverToken;


    //Initialisation

    /// <summary>
    /// Obtain server information from passed in server token and get references
    /// </summary>
    public void Initialise(ServerInfoToken serverToken, TMP_InputField nameField, TMP_InputField passwordField, OnlineMenuController onlineMenuController)
    {
        _clientName = nameField;
        _clientPassword = passwordField;
        _menuController = onlineMenuController;

        _serverName.text = serverToken.Name;
        _playerName.text = serverToken.HostName;
        _playerCount.text = serverToken.Players + "/" + serverToken.MaxPlayers;

        _serverToken = serverToken;

        if (serverToken.RequiresPassword)
        {
            _passwordRequired.text = "PASSWORD REQUIRED";
        }
        else
        {
            _passwordRequired.text = "OPEN";
        }
        
    }


    //Methods

    /// <summary>
    /// Attempt to join the server associated with the token provided
    /// </summary>
    public void ButtonPress()
    {
        if(_clientName.text == "")
        {
            StaticUIEffects.SendNotification("Please enter a player name.");
        }
        else if(_serverToken.RequiresPassword && _clientPassword.text == "")
        {
            StaticUIEffects.SendNotification("This server requires a password");
        }
        else
        {
            _menuController.ClientJoinLobby(_serverToken.Id, _clientName.text, _clientPassword.text);
        }
    }

}
