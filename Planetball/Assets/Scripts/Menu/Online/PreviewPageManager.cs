﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Class to create and populate pages of lobby list

public class PreviewPageManager : MonoBehaviour {

    //Prefab references
    [SerializeField] GameObject _previewPageObject;
    [SerializeField] GameObject _previewObject;

    //References
    [Header("Component References")]
    [SerializeField] LobbyList _lobbyList;
    [SerializeField] UICounter _pageCounter;
    [SerializeField] MenuSwitcher _menuSwitcher;
    [SerializeField] Transform _leftMenu;
    [SerializeField] OnlineMenuController _menuController;

    [Header("Input references")]
    [SerializeField] TMPro.TMP_InputField _nameField;
    [SerializeField] TMPro.TMP_InputField _passwordField;

    List<Transform> _pages;

    //Variables

    [Header("Alignment")]
    [SerializeField] float _xoffset = 0f;
    [SerializeField] float _yoffset = 0f;
    [SerializeField] float _ystep = -90f;


    //Initialisation

    private void Start()
    {
        _pages = new List<Transform>();
    }

    //Update

    /// <summary>
    /// Keep track of which page should be active
    /// </summary>
    private void FixedUpdate()
    {
        if(_pageCounter.Value > 0 && _pageCounter.Value != _menuSwitcher.Page + 1)
        {
            _menuSwitcher.SwapToMenu(_pageCounter.Value - 1);
        }
    }


    //List editing

    /// <summary>
    /// Close current preview list and create new lists
    /// </summary>
    public void RefreshList()
    {
        _menuSwitcher.CloseMenu(true, SetNewList);
    }
    /// <summary>
    /// Replace current server pages with new server pages
    /// </summary>
    void SetNewList()
    {
        ClearOldPages();
        PopulateServerList();
    }
    /// <summary>
    /// Destroy old pages
    /// </summary>
    void ClearOldPages()
    {
        for (int i = _pages.Count - 1; i >= 0; --i)
        {
            Destroy(_pages[i].gameObject);
        }
        _pages = new List<Transform>();

        _menuSwitcher.EmptyList();
    }
    /// <summary>
    /// Instantiate pages and server previews, set page 1 active
    /// </summary>
    void PopulateServerList()
    {
        _pages = new List<Transform>();
        
        List<ServerInfoToken> servers = _lobbyList.ServerTokens;
        Transform currentPage = null;

        for (int i = 0; i < servers.Count; ++i)
        {
            if (i >= _pages.Count * 4)
            {
                GameObject page = Instantiate(_previewPageObject, _leftMenu);
                _pages.Add(page.transform);
                currentPage = page.transform;

                _menuSwitcher.AddPage(currentPage.GetComponentInChildren<UISlideInOut>());
            }

            GameObject preview = Instantiate(_previewObject, currentPage);
            preview.transform.localPosition = new Vector3(_xoffset, (i - (_pages.Count - 1) * 4) * _ystep + _yoffset, 0f);
            ServerPreviewController previewController = preview.GetComponentInChildren<ServerPreviewController>();

            previewController.Initialise(servers[i], _nameField, _passwordField, _menuController);
        }

        for (int i = 1; i < _pages.Count; ++i)
        {
            _pages[i].gameObject.SetActive(false);
        }
        _menuSwitcher.SwapToMenu(0);
        _pageCounter.MaxValue = _pages.Count;
        _pageCounter.MinValue = Mathf.Min(1, _pages.Count);

        _pageCounter.Value = 1;
    }

}
