﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using Bolt;
using UdpKit;

//Script to manage the online menu

public class OnlineMenuController : Bolt.GlobalEventListener {

    //References
    [SerializeField] MenuSwitcher _menu;
    [SerializeField] UISlideInOut _logo;
    [SerializeField] UISlideInOut _sidePanel;

    //Host menu references
    [SerializeField] TMP_InputField _hostName;
    [SerializeField] TMP_InputField _serverName;
    [SerializeField] TMP_InputField _hostPassword;
    [SerializeField] TMP_InputField _maxPlayers;

    //Client menu references
    [SerializeField] TMP_InputField _clientPassword;
    [SerializeField] LobbyList _lobbyList;

    //Static flags

    public static bool _backgroundPreEnter = false;

    //Variables

    bool _waitingForBoltShutdown = false;
    System.Action _postShutdownMethod;


    //Initialisation

    /// <summary>
    /// Handle music and initial positions of UI elements
    /// </summary>
    private void Start()
    {
        Audio.MusicPlayer.PlayTrack(Audio.Music.MENU);

        if (_backgroundPreEnter)
        {
            _backgroundPreEnter = false;

            _logo.SetToOrigin();
            _sidePanel.SetToOrigin();
        }
    }


    //Update

    /// <summary>
    /// Check for bolt shutdown if currently waiting and carry out queued action 
    /// </summary>
    private void FixedUpdate()
    {
        if(_waitingForBoltShutdown && !BoltNetwork.IsRunning)
        {
            _waitingForBoltShutdown = false;
            StaticUIEffects.StopLoadIcon();

            if (_postShutdownMethod != null)
            {
                _postShutdownMethod.Invoke();
                _postShutdownMethod = null;
            }
        }
    }


    //Front buttons

    /// <summary>
    /// Tell the host menu to enter if bolt is not running, or queue up method to run after shutdown
    /// </summary>
    public void MenuHostButton()
    {
        if(!BoltNetwork.IsRunning)
        {
            _menu.SwapToMenu(1, 0.4f);
        }
        else
        {
            try
            {
                BoltLauncher.Shutdown();
            }
            catch { }
            _waitingForBoltShutdown = true;
            StaticUIEffects.StartLoadIcon();
            _postShutdownMethod = MenuHostButton;
        }
    }
    /// <summary>
    /// Tell the client menu to enter if bolt is not running and start up as client
    /// </summary>
    public void MenuJoinButton()
    {
        if (!BoltNetwork.IsRunning)
        {
            _logo.Exit();
            _menu.SwapToMenu(2, 0.4f);

            BoltLauncher.StartClient();
        }
        else
        {
            try
            {
                BoltLauncher.Shutdown();
            }
            catch { }
            _waitingForBoltShutdown = true;
            StaticUIEffects.StartLoadIcon();
            _postShutdownMethod = MenuJoinButton;
        }
    }
    /// <summary>
    /// Close the online menu and return to main menu
    /// </summary>
    public void MenuBackButton()
    {
        if(!BoltNetwork.IsRunning)
        {
            MenuController._backgroundPreEnter = true;
            _menu.CloseMenu();
            StartCoroutine(DelaySceneLoad("MenuScene", 0.4f));
        }
        else
        {
            try
            {
                BoltLauncher.Shutdown();
            }
            catch { }
            _waitingForBoltShutdown = true;
            StaticUIEffects.StartLoadIcon();
            _postShutdownMethod = MenuBackButton;
        }
    }


    //Host menu

    /// <summary>
    /// Exit menu and begin hosting using the details provided
    /// </summary>
    public void HostStartButton()
    {
        ConnectionManager.ServerName = _serverName.text;
        ConnectionManager.HostName = _hostName.text;
        ConnectionManager.Password = _hostPassword.text;
        PlayerList.MaxPlayers = int.Parse(_maxPlayers.text);

        if(_serverName.text == "" || _hostName.text == "")
        {
            StaticUIEffects.SendNotification("Ensure name fields are not empty.");
        }
        else
        {
            _menu.CloseMenu();
            StartCoroutine(DelayMethod(_logo.Exit, 0.4f));
            StartCoroutine(DelayMethod(_sidePanel.Exit, 0.4f));

            StartCoroutine(DelayMethod(HostStartServer, 0.6f));
        }
    }
    /// <summary>
    /// Return to front of online menu
    /// </summary>
    public void HostBackButton()
    {
        _menu.SwapToMenu(0, 0.4f);
    }
    /// <summary>
    /// Simply start the server 
    /// </summary>
    void HostStartServer()
    {
        BoltLauncher.StartServer();
    }


    //Client menu

    /// <summary>
    /// Return to front of online menu
    /// </summary>
    public void ClientBackButton()
    {
        _menu.SwapToMenu(0, 0.4f);
        _logo.Enter();
        BoltLauncher.Shutdown();
    }
    /// <summary>
    /// Exit menu and join lobby
    /// </summary>
    public void ClientJoinLobby(System.Guid id, string name, string password)
    {
        _menu.CloseMenu();
        StartCoroutine(DelayMethod(_logo.Exit, 0.4f));
        StartCoroutine(DelayMethod(_sidePanel.Exit, 0.4f));

        StartCoroutine(DelayConnection(id, name, password, 0.6f));
    }
    


    //Callbacks

    /// <summary>
    /// If connection is refused while on the online menu, disconnect and return to the front menu
    /// </summary>
    public override void ConnectRefused(UdpEndPoint endpoint, IProtocolToken token)
    {
        BoltLauncher.Shutdown();
        _menu.SwapToMenu(0);
        StaticUIEffects.StopLoadIcon();

        if(!_logo.gameObject.activeSelf)
        {
            _logo.Enter();
        }
        if(!_sidePanel.gameObject.activeSelf)
        {
            _sidePanel.Enter();
        }
    }


    //Coroutines

    /// <summary>
    /// Run method after delay seconds
    /// </summary>
    IEnumerator DelayMethod(System.Action method, float delay)
    {
        yield return new WaitForSeconds(delay);

        method.Invoke();
    }
    /// <summary>
    /// Load a scene after delay seconds
    /// </summary>
    IEnumerator DelaySceneLoad(string scene, float delay)
    {
        yield return new WaitForSeconds(delay);

        SceneManager.LoadSceneAsync(scene);
    }
    /// <summary>
    /// Connect to a server after a delay
    /// </summary>
    IEnumerator DelayConnection(System.Guid id, string name, string password, float delay)
    {
        yield return new WaitForSeconds(delay);

        _lobbyList.JoinServer(id, name, password);
        StaticUIEffects.StartLoadIcon();
    }
}
