﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using Bolt;

//Class is responsible for maintaining the appearance of the lobby
public class LobbyMenu : GlobalEventListener
{

    //References

    [SerializeField] UISlideInOut _sidePanel;
    [SerializeField] MenuSwitcher _menu;
    [SerializeField] TextMeshProUGUI _lobbyTitle;
    [SerializeField] UISlideInOut _blueReadySlide;
    [SerializeField] UISlideInOut _redReadySlide;
    [SerializeField] UISlideInOut _readyButtonSlide;

    //Variables

    bool _leavingLobby = false;

    
    //Initialisation

    private void Start()
    {
        Audio.VoicePlayer.FadeOutTrack(1f);
        PlayerList.OnListUpdated.AddListener(ReadyButtonEnterExit);
        PlayerReadyTracker.OnReadyChanged.AddListener(ReadyTagEnterExit);
        ReadyButtonEnterExit();
        ReadyTagEnterExit();
        _lobbyTitle.text = ConnectionManager.ServerName;
    }
    private void OnDestroy()
    {
        PlayerList.OnListUpdated.RemoveListener(ReadyButtonEnterExit);
        PlayerReadyTracker.OnReadyChanged.RemoveListener(ReadyTagEnterExit);
    }


    //Update

    /// <summary>
    /// Send message to leave lobby if bolt has closed due to connection loss
    /// or lobby opened without internet
    /// </summary>
    private void FixedUpdate()
    {
        if(!_leavingLobby && !BoltNetwork.IsRunning)
        {
            _leavingLobby = true;
            StaticUIEffects.SendNotification("Connection lost. Click to return to menu.", methodOnDismissal: LeaveLobby);
        }
    }


    //Methods

    /// <summary>
    /// Starts game if called by host/player while players are ready
    /// </summary>
    public void StartGame()
    {
        bool hostOrPlayer = (BoltNetwork.IsServer || PlayerList.LocalPlayer.IsBlue || PlayerList.LocalPlayer.IsRed);
        if (hostOrPlayer && PlayerReadyTracker.BlueReady && PlayerReadyTracker.RedReady)
        {
            GameStart evnt = GameStart.Create();
            evnt.Send();
        }
    }
    /// <summary>
    /// Load into the game scene
    /// </summary>
    public void LoadGame()
    {
        if (BoltNetwork.IsServer)
        {
            PlayerReadyTracker.CancelReady();
            BoltNetwork.LoadScene("OnlineGameScene");
        }
    }

    /// <summary>
    /// Button to leave the lobby and return to the online menu
    /// </summary>
    public void LeaveLobbyButton()
    {
        _leavingLobby = true;
        if (BoltNetwork.IsRunning)
        {
            BoltLauncher.Shutdown();
        }

        LeaveLobby();
    }
    //
    void LeaveLobby()
    {
        _menu.CloseMenu();
        StartCoroutine(DelayMethod(_sidePanel.Exit, 0.4f));
        StartCoroutine(DelayMethod(OpenOnlineMenu, 0.6f));
    }
    void OpenOnlineMenu()
    {
        SceneManager.LoadScene("OnlineMenu");
    }

    /// <summary>
    /// Swap to the settings menu
    /// </summary>
    public void OpenSettingsMenu()
    {
        _menu.SwapToMenu(1, 0.4f);
    }
    /// <summary>
    /// Return to the main lobby menu from the settings menu, and sync settings if server
    /// </summary>
    public void CloseSettingsMenu()
    {
        _menu.SwapToMenu(0, 0.4f);

        if (BoltNetwork.IsServer)
        {
            GameSettings.SendSettingsToClient();
        }
    }

    /// <summary>
    /// When pressed, flip the readiness of the local player via event
    /// </summary>
    public void ReadyButton()
    {
        if (PlayerList.LocalPlayer.IsBlue)
        {
            PlayerReadyTracker.SetBlueReady(!PlayerReadyTracker.BlueReady);
        }
        else if (PlayerList.LocalPlayer.IsRed)
        {
            PlayerReadyTracker.SetRedReady(!PlayerReadyTracker.RedReady);
        }
    }
    /// <summary>
    /// Sets both players to not ready
    /// </summary>
    public void StopReady()
    {
        PlayerReadyTracker.CancelReady();
    }


    //Coroutines

    IEnumerator DelayMethod(System.Action method, float delay)
    {
        yield return new WaitForSeconds(delay);

        method.Invoke();
    }


    //Callbacks

    /// <summary>
    /// Call exit/enter on ready button when appropriate
    /// </summary>
    void ReadyButtonEnterExit()
    {
        if (PlayerList.IsLocalPlaying && !_readyButtonSlide.gameObject.activeSelf)
        {
            _readyButtonSlide.Enter();
        }
        else if (!PlayerList.IsLocalPlaying && _readyButtonSlide.gameObject.activeSelf)
        {
            _readyButtonSlide.Exit();
        }
    }
    /// <summary>
    /// Call exit/enter on readiness tags when appropriate
    /// </summary>
    void ReadyTagEnterExit()
    {
        if (PlayerReadyTracker.BlueReady && !_blueReadySlide.gameObject.activeSelf)
        {
            _blueReadySlide.Enter();
        }
        else if (!PlayerReadyTracker.BlueReady && _blueReadySlide.gameObject.activeSelf)
        {
            _blueReadySlide.Exit();
        }

        if (PlayerReadyTracker.RedReady && !_redReadySlide.gameObject.activeSelf)
        {
            _redReadySlide.Enter();
        }
        else if (!PlayerReadyTracker.RedReady && _redReadySlide.gameObject.activeSelf)
        {
            _redReadySlide.Exit();
        }
    }


    //Bolt Callbacks

    /// <summary>
    /// Sync up player readiness when new player connects
    /// </summary>
    public override void Connected(BoltConnection connection)
    {
        if (BoltNetwork.IsServer)
        {
            PlayerReadyTracker.SyncReadiness();
        }
    }
    /// <summary>
    /// Cause menu to exit and gamescene to load if host
    /// </summary>
    public override void OnEvent(GameStart evnt)
    {
        if (BoltNetwork.IsServer)
        {
            StartCoroutine(DelayMethod(LoadGame, 0.8f));
        }

        _menu.CloseMenu();
        StartCoroutine(DelayMethod(_sidePanel.Exit, 0.4f));
    }
    /// <summary>
    /// Return to online menu on disconnect
    /// </summary>
    public override void Disconnected(BoltConnection connection)
    {
        if(!BoltNetwork.IsServer)
        {
            _leavingLobby = true;
            StaticUIEffects.SendNotification("Disconnected from host. Click to return to menu.", methodOnDismissal: LeaveLobby);
        }
    }

}
