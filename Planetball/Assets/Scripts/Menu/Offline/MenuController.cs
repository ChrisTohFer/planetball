﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

//Class provides methods for menu button presses

public class MenuController : MonoBehaviour {

    //References

    [Header("Animator References")]
    [SerializeField] UISlideInOut _logo;
    [SerializeField] UISlideInOut _sidePanel;

    [SerializeField] MenuSwitcher _frontMenu;
    [SerializeField] MenuSwitcher _optionsMenu;
    
    //Static flags

    public static bool _backgroundPreEnter = false;


    //Scene start and end management

    /// <summary>
    /// Start music (and commentary if this is the first instance of the scene)
    /// </summary>
    private void Start()
    {
        Audio.MusicPlayer.PlayTrack(Audio.Music.MENU);
        Audio.VoicePlayer.FadeOutTrack(1f);
        if(StaticInfo.MenuScene == 0)
        {
            //Play voice line (if intended to be on menu)
        }
        if(_backgroundPreEnter)
        {
            _backgroundPreEnter = false;

            _logo.SetToOrigin();
            _sidePanel.SetToOrigin();
        }

        StaticInfo.MenuScene++;
    }
    /// <summary>
    /// Stop commentary when menu is left
    /// </summary>
    private void OnDestroy()
    {
        Audio.VoicePlayer.Stop();
    }


    //Front menu buttons

    /// <summary>
    /// Play the local 2 player game
    /// </summary>
    public void MainPlayGame()
    {
        _frontMenu.CloseMenu();
        StartCoroutine(DelayMethod(MenuExit, 0.4f));
        StartCoroutine(DelaySceneLoad("LocalGameScene", 0.7f));
    }
    /// <summary>
    /// Go to the online menu
    /// </summary>
    public void MainOnline()
    {
        _frontMenu.CloseMenu();
        OnlineMenuController._backgroundPreEnter = true;
        StartCoroutine(DelaySceneLoad("OnlineMenu", 0.4f));
    }
    /// <summary>
    /// Quit the game
    /// </summary>
    public void MainQuit()
    {
        Application.Quit();
    }
    /// <summary>
    /// Open the options menu
    /// </summary>
    public void MainOptions()
    {
        _frontMenu.SwapToMenu(1, 0.2f);
    }


    //Options buttons

    /// <summary>
    /// Close currently open options menu, open settings
    /// </summary>
    public void OpenSettings()
    {
        if(_optionsMenu.Page == 1) ControlsButton.DeselectAll();
        _optionsMenu.SwapToMenu(0, 0.2f);
    }
    /// <summary>
    /// Close currently open options menu, open controls
    /// </summary>
    public void OpenControls()
    {
        _optionsMenu.SwapToMenu(1, 0.2f);
    }
    /// <summary>
    /// Close currently open options menu, open audio
    /// </summary>
    public void OpenAudio()
    {
        if (_optionsMenu.Page == 1) ControlsButton.DeselectAll();
        _optionsMenu.SwapToMenu(2, 0.2f);
    }
    /// <summary>
    /// Close the options menu and go back to the front menu, save all options
    /// </summary>
    public void OptionsBack()
    {
        _optionsMenu.CloseMenu();
        _frontMenu.SwapToMenu(0, 0.2f);

        Controls.Save();
        Options.O.SaveToFile();
        GameSettings.SaveToFile();
    }


    //Transitions

    /// <summary>
    /// Background menu elements exit the screen
    /// </summary>
    private void MenuExit()
    {
        _logo.Exit();
        _sidePanel.Exit();
    }

    
    //Delay coroutine

    /// <summary>
    /// Run a method (no parameters or return type) after delay seconds
    /// </summary>
    IEnumerator DelayMethod(Action method, float delay)
    {
        yield return new WaitForSeconds(delay);

        method.Invoke();
    }
    /// <summary>
    /// Load a scene after delay seconds
    /// </summary>
    IEnumerator DelaySceneLoad(string scene, float delay)
    {
        yield return new WaitForSeconds(delay);

        SceneManager.LoadSceneAsync(scene);
    }

}
