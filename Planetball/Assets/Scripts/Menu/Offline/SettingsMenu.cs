﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Class to manage the separate settings pages

public class SettingsMenu : MonoBehaviour {

    //References

    [SerializeField] MenuSwitcher _menu;
    [SerializeField] BoxSlider _pageSlider;

    //Properties

    [SerializeField] float _overlap;

    //Update

    private void FixedUpdate()
    {
        if(_pageSlider.IntValue != _menu.Page)
        {
            _menu.SwapToMenu(_pageSlider.IntValue, _overlap);
        }
    }
    
}
