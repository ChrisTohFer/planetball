﻿using UnityEngine;
using UnityEngine.UI;

//

public class OptionsMenu : MonoBehaviour {

    //Menu components
    public BoxSlider MasterVol;
    public BoxSlider MusicVol;
    public BoxSlider VoiceVol;
    public BoxSlider EffectsVol;
    public BoxSlider ScreamVol;

    //Set sliders to appropriate values when options menu is opened
    private void OnEnable()
    {
        MasterVol.Value = Options.O.MasterVolume;
        MusicVol.Value = Options.O.MusicVolume;
        VoiceVol.Value = Options.O.VoiceVolume;
        EffectsVol.Value = Options.O.EffectVolume;
        ScreamVol.Value = Options.O.ScreamVolume;
    }

}
