﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Audio
{

    public class VoicePlayer : MonoBehaviour
    {
        //For static access
        static VoicePlayer _singleton;

        //Editable properties
        [SerializeField] AudioSource _source;
        [SerializeField] VoiceTrack[] _trackList;
        float _volume = 1f;
        public static float Volume
        {
            get { return _singleton._volume; }
            set
            {
                _singleton._source.volume = value;
                _singleton._volume = value;
            }
        }

        //Internal
        Dictionary<VoiceType, VoiceTrack> _voiceDictionary;

        //Variables
        VoiceTrack _currentTrack;
        VoiceTrack _queuedTrack;
        bool _currentlyPlaying;
        float _lastPlayTime;

        public static float SilentTime
        {
            get
            {
                if(_singleton._currentlyPlaying)
                {
                    return 0f;
                }
                else
                {
                    return Time.time - _singleton._lastPlayTime;
                }
            }
        }   //The time since the previous voice line ended
        
        //Unity Methods

        //Awake sets static reference, initialises the dictionary and sets audiosource for all tracks
        private void Awake()
        {
            if(_singleton == null)
            {
                _singleton = this;
                DontDestroyOnLoad(gameObject);

                Initialize();
            }
            else
            {
                //Destroy(gameObject); Destroy on start due to bolt functionality
            }
        }
        private void Start()
        {
            if(_singleton != this)
            {
                Destroy(gameObject);
            }
        }
        private void Initialize()
        {
            _voiceDictionary = new Dictionary<VoiceType, VoiceTrack>();
            _lastPlayTime = Time.time;

            foreach (VoiceTrack vt in _trackList)
            {
                vt.Source = _source;
                _voiceDictionary.Add(vt.Type, vt);
            }
        }

        //Fixed update checks play state and calls relevant events
        private void FixedUpdate()
        {
            if(_currentlyPlaying && !_currentTrack.IsPlaying)
            {
                _currentlyPlaying = false;
                OnCurrentTrackFinished();
            }
        }


        //Other methods

        //Play voice track depending on priority, will sync voice lines if online
        public static void PlayTrack(VoiceType type)
        {
            if(!BoltNetwork.IsRunning || BoltNetwork.IsServer)  //Client plays voice events recieved from host
            {
                VoiceTrack track = _singleton._voiceDictionary[type];
                if (!_singleton._currentlyPlaying)
                {
                    _singleton.Play(track);
                }
                else if(track.Priority > _singleton._currentTrack.Priority)
                {
                    //maybe fade and queue if sudden stop sounds wrong
                    _singleton.Play(track);
                }
                else if(track.Priority == _singleton._currentTrack.Priority && track.IsQueueable)
                {
                    _singleton._queuedTrack = track;
                }
            }
            
        }
        void Play(VoiceTrack track)
        {
            int subtrack = track.PlayRandom();
            _currentlyPlaying = true;
            _currentTrack = track;

            if (BoltNetwork.IsRunning && BoltNetwork.IsServer)
            {
                PlayVoice evnt = PlayVoice.Create();
                evnt.Type = (int)track.Type;
                evnt.SubTrack = subtrack;
                evnt.Send();
            }
        }

        //Play called when track has required priority and is ready to play immediately
        public static void PlayLocal(VoiceType type, int subtrack = -1)
        {
            Stop();

            VoiceTrack track = _singleton._voiceDictionary[type];
            if (subtrack == -1)
            {
                track.PlayRandom();
            }
            else
            {
                track.Play(subtrack);
            }

            _singleton._currentlyPlaying = true;
            _singleton._currentTrack = track;
        }

        //Stop the current track and clear queue
        public static void Stop()
        {
            if(_singleton != null && _singleton._source != null)
            {
                _singleton._source.Stop();
                _singleton._currentlyPlaying = false;
                _singleton._queuedTrack = null;
                _singleton._source.volume = _singleton._volume;
                _singleton.StopAllCoroutines();
            }
            
        }


        //Coroutines

        //Play track after a delay
        public static void PlayInSeconds(VoiceType type, float wait)
        {
            _singleton.StartCoroutine(_singleton.CPlayInSeconds(type, wait));
        }
        IEnumerator CPlayInSeconds(VoiceType type, float wait)
        {
            yield return new WaitForSeconds(wait);
            PlayTrack(type);
        }

        //Fade out current track
        public static void FadeOutTrack(float fadeDuration)
        {
            if(_singleton._currentlyPlaying)
            {
                _singleton.StartCoroutine(_singleton.CFadeOutTrack(fadeDuration));
            }
        }
        IEnumerator CFadeOutTrack(float fadeDuration)
        {
            float fadeProgress = 0f;

            while(fadeProgress < 1f && _source.isPlaying)
            {
                fadeProgress += Time.fixedDeltaTime / fadeDuration;
                _source.volume = Volume * (1f - fadeProgress);
                yield return new WaitForFixedUpdate();
            }

            _source.Stop();
            _source.volume = Volume;
        }

        //Events

        //Called when track is finished, can be used to play a queued track
        void OnCurrentTrackFinished()
        {
            if(_queuedTrack != null)
            {
                Play(_queuedTrack);
                _queuedTrack = null;
            }

            _lastPlayTime = Time.time;
        }

        //Callback moved to MiscEventTracker
        /*public override void OnEvent(PlayVoice evnt)
        {
            PlayLocal((VoiceType)evnt.Type, evnt.SubTrack);
        }*/

    }

}