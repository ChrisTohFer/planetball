﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Audio
{

    public enum VoiceType
    {
        START,
        BANTER,

        BLUE_HIT,
        BLUE_HIT_BY_RED,
        BLUE_HIT_BIG,
        BLUE_HIT_SELF,
        BLUE_HEALTH_50,
        BLUE_HEALTH_10,
        BLUE_NOT_HIT,
        BLUE_WIN,
        BLUE_WIN_PERFECT,

        RED_HIT,
        RED_HIT_BY_BLUE,
        RED_HIT_BIG,
        RED_HIT_SELF,
        RED_HEALTH_50,
        RED_HEALTH_10,
        RED_NOT_HIT,
        RED_WIN,
        RED_WIN_PERFECT

    }

    [CreateAssetMenu(fileName = "NewVoiceTrack", menuName = "VoiceTrack")]
    public class VoiceTrack : ScriptableObject
    {
        //Editable properties
        [SerializeField] VoiceType _type;
        [SerializeField] int _priority = 0;
        [SerializeField] bool _isQueueable = false;
        [SerializeField] AudioClip[] _voiceClips;

        public VoiceType Type
        {
            get { return _type; }
        }
        public int Priority
        {
            get { return _priority; }
        }
        public bool IsQueueable
        {
            get { return _isQueueable; }
        }
        public int ClipCount
        {
            get { return _voiceClips.Length; }
        }
        public bool IsPlaying
        {
            get { return Source.isPlaying; }
        }

        AudioSource _source;
        public AudioSource Source
        {
            get { return _source; }
            set { _source = value; }
        }


        //Play methods; Play random returns int for syncing the voice line played between players

        public void Play(int subtrack)
        {
            if(subtrack < ClipCount)
            {
                Source.Stop();
                Source.clip = _voiceClips[subtrack];
                Source.Play();
            }
        }
        public int PlayRandom()
        {
            int subtrack = Random.Range(0, ClipCount);
            Play(subtrack);
            return subtrack;
        }


        //Internal

    }

}