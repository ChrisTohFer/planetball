﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Audio
{

    public class MusicPlayer : MonoBehaviour
    {
        //Global static reference
        public static MusicPlayer _singleton;

        //Editable track list
        [SerializeField] MusicTrack[] _tracks;

        //Internal dictionary and list of playing tracks
        Dictionary<Music, MusicTrack> _musicDictionary;
        List<MusicTrack> _currentlyPlaying;

        //Properties
        float _volume = 1f;
        public static float Volume
        {
            get { return _singleton._volume; }
            set
            {
                _singleton._volume = value;
                foreach(MusicTrack track in _singleton._currentlyPlaying)
                {
                    track.Volume = value;
                }
            }
        }

        //Initialisation

        /// <summary>
        /// Set singleton reference and initialise all music tracks
        /// </summary>
        private void Awake()
        {
            if(_singleton == null)
            {
                _singleton = this;
                DontDestroyOnLoad(gameObject);

                Initialise();
            }
            else
            {
                Destroy(gameObject);
            }
        }
        /// <summary>
        /// Initialise all music tracks
        /// </summary>
        void Initialise()
        {
            _musicDictionary = new Dictionary<Music, MusicTrack>();
            _currentlyPlaying = new List<MusicTrack>();

            for(int i = 0; i < _tracks.Length; ++i)
            {
                MusicTrack track = _tracks[i];
                _musicDictionary.Add(track.Id, track);
                track.Initialise(gameObject);
            }
        }

        //Update stuff

        /// <summary>
        /// Tracks manage their own looping etc in their fixedupdate, which must be called here
        /// Remove tracks which are not playing
        /// </summary>
        private void FixedUpdate()
        {
            for(int i = _currentlyPlaying.Count - 1; i >= 0; --i)
            {
                MusicTrack track = _currentlyPlaying[i];

                track.FixedUpdate();

                if(!track.IsPlaying)
                {
                    _currentlyPlaying.Remove(track);
                }
            }
        }

        //Methods

        /// <summary>
        /// Begins playing the selected track
        /// </summary>
        public static void PlayTrack(Music track)
        {
            bool inList = false;
            foreach (MusicTrack otherTrack in _singleton._currentlyPlaying)
            {
                if (otherTrack.Id == track)
                {
                    if(otherTrack.IsFading)
                    {
                        otherTrack.Stop();
                    }
                    else
                    {
                        inList = true;
                    }
                }
            }

            if(!inList)
            {
                FadeOutAll();

                MusicTrack newTrack = _singleton._musicDictionary[track];
                _singleton._currentlyPlaying.Add(newTrack);

                newTrack.Volume = Volume;
                newTrack.Play();
            }

        }
        /// <summary>
        /// Fade in chosen track over default duration
        /// </summary>
        public static void FadeInTrack(Music track)
        {
            bool inList = false;
            foreach (MusicTrack otherTrack in _singleton._currentlyPlaying)
            {
                if(otherTrack.Id == track)
                {
                    inList = true;
                }
            }

            if(!inList)
            {
                MusicTrack newTrack = _singleton._musicDictionary[track];
                _singleton._currentlyPlaying.Add(newTrack);

                newTrack.Volume = Volume;
                newTrack.FadeIn();

                //Sync time with the first track in the current list; assumes that all will have same time value
                newTrack.Time = _singleton._currentlyPlaying[0].Time;
            }
        }
        /// <summary>
        /// Fade out music track over chosen duration
        /// </summary>
        public static void FadeOutTrack(Music track)
        {
            foreach (MusicTrack fadeTrack in _singleton._currentlyPlaying)
            {
                if (fadeTrack.Id == track)
                {
                    fadeTrack.FadeOut();
                }
            }
        }
        /// <summary>
        /// Fade out every track over chosen duration (default if not specified)
        /// </summary>
        public static void FadeOutAll(float fadeDuration = -1f)
        {
            if (fadeDuration >= 0f)
            {
                foreach(MusicTrack track in _singleton._currentlyPlaying)
                {
                    track.FadeOut(fadeDuration);
                }
            }
            else
            {
                foreach (MusicTrack track in _singleton._currentlyPlaying)
                {
                    track.FadeOut();
                }
            }
        }
        /// <summary>
        /// Stops every track instantly
        /// </summary>
        public static void StopAll()
        {
            for (int i = _singleton._currentlyPlaying.Count - 1; i >= 0; --i)
            {
                MusicTrack track = _singleton._currentlyPlaying[i];
                track.Stop();
                track.Volume = 0f;
                
                _singleton._currentlyPlaying.Remove(track);
            }
        }

    }

}