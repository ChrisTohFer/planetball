﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Audio
{
    public enum Music
    {
        MENU,
        GAMEPLAY_1,
        GAMEPLAY_2,
        GAMEPLAY_3,
        GAMEPLAY_END
    }
    
    [CreateAssetMenu(fileName = "NewMusicTrack", menuName = "MusicTrack")]
    public class MusicTrack : ScriptableObject
    {
        //Editable properties
        [SerializeField] Music _id;
        [SerializeField] float _defaultFadeTime = 1f;
        [SerializeField] AudioClip _clip;

        [Header("Looping")]
        [SerializeField] bool _loops = true;
        [SerializeField] float _loopStart = 0f;
        [SerializeField] float _loopEnd = 0f;

        //Internal

        AudioSource _source;
        float _volume = 1f;
        float _volumeScale = 1f;
        float _currentFadeRate = 0f;

        //Public properties
        public Music Id
        {
            get { return _id; }
        }
        public float Volume
        {
            get { return _volume; }
            set
            {
                _volume = value;
                _source.volume = value * _volumeScale;
            }
        }
        public bool IsPlaying
        {
            get { return (_source.isPlaying && _volumeScale != 0f); }
        }
        public float VolumeScale
        {
            get { return _volumeScale; }
            set
            {
                _volumeScale = value;
                _source.volume = _volume * _volumeScale;
            }
        }
        public float Time
        {
            get { return _source.time; }
            set { _source.time = value; }
        }
        public bool IsFading
        {
            get
            {
                return _currentFadeRate < 0f;
            }
        }

        //Initialize

        public void Initialise(GameObject g)
        {
            _source = g.AddComponent<AudioSource>();

            _source.playOnAwake = false;
            _source.loop = _loops;
            _source.volume = _volume * _volumeScale;
            _source.clip = _clip;
        }

        //Update stuff

        /// <summary>
        /// FixedUpdate method manages the music elements which are not controlled by the source, such as fading
        /// CALLED FROM MUSICPLAYER FIXEDUPDATE, NOT UNITY
        /// </summary>
        public void FixedUpdate()
        {
            //Loop at appropriate timestamp if enabled and chosen
            if(_loops && _loopEnd > 0f && _source.time > _loopEnd)
            {
                _source.time -= _loopEnd - _loopStart;
            }

            //Apply volume change for fading in/out
            if(_currentFadeRate != 0f)
            {
                VolumeScale += _currentFadeRate * UnityEngine.Time.fixedDeltaTime;
            }

            //Cancel fading if finished
            if(VolumeScale > 1f)
            {
                VolumeScale = 1f;
                _currentFadeRate = 0f;
            }
            else if(VolumeScale < 0f)
            {
                VolumeScale = 0f;
                _currentFadeRate = 0f;
            }
        }

        //Methods

        /// <summary>
        /// Play the track from the specified start time, does nothing if already playing
        /// </summary>
        public void Play(float startTime = 0f)
        {
            if(!IsPlaying)
            {
                _currentFadeRate = 0f;
                VolumeScale = 1f;
                _source.Play();
                _source.time = startTime;
            }
            else if(!IsFading)
            {
                _currentFadeRate = -_currentFadeRate;
            }
            else
            {
                Debug.Log("Music track already playing");
            }
        }
        /// <summary>
        /// Stop the track from playing completely
        /// </summary>
        public void Stop()
        {
            _source.Stop();
            VolumeScale = 1f;
            _defaultFadeTime = 0f;
        }
        /// <summary>
        /// Fade in the track over the duration specified, or the default duration
        /// </summary>
        public void FadeIn(float duration = -1f, float startTime = 0f)
        {
            if (duration < 0) { duration = _defaultFadeTime; }

            if (duration != 0f)
            {
                _currentFadeRate = 1f / duration;
            }
            else
            {
                _currentFadeRate = 10000f;
            }

            //Begin playing for fade in if not currently playing
            if(!IsPlaying)
            {
                _source.Play();
                _source.time = startTime;
                VolumeScale = _currentFadeRate * UnityEngine.Time.fixedDeltaTime;
            }
        }
        /// <summary>
        /// Fade out the track over the duration specified, or the default duration
        /// </summary>
        public void FadeOut(float duration = -1f)
        {
            if (duration < 0) { duration = _defaultFadeTime; }

            if (duration != 0f)
            {
                _currentFadeRate = 1f / duration;
            }
            else
            {
                _currentFadeRate = 10000f;
            }

            _currentFadeRate *= -1f;    //Negative rates for fading out
        }


        /*
        //Editable properties

        [SerializeField] Music _id;
        [SerializeField] float _defaultFadeTime = 0f;
        [SerializeField] AudioClip[] _layeredClips;

        [Header("Looping")]
        [SerializeField] bool _loops = true;
        [SerializeField] float _loopStart = 0f;
        [SerializeField] float _loopEnd = 0f;

        AudioSource[] _sources;
        float[] _layerVolume;
        float _volume = 1f;
        bool _isPlaying = false;

        public Music Id
        {
            get { return _id; }
        }
        public float Volume
        {
            get { return _volume; }
            set
            {
                _volume = value;
                for (int i = 0; i < _sources.Length; ++i)
                {
                    AudioSource a = _sources[i];
                    a.volume = value * _layerVolume[i];
                }
            }
        }

        public bool IsPlaying
        {
            get { return _isPlaying; }
        }
        public int LayerCount
        {
            get { return _sources.Length; }
        }

        //Initialisation


        //Methods

        /// <summary>
        /// Set the volume of a subtrack
        /// </summary>
        void SetLayerVolume(int layer, float volume)
        {
            _layerVolume[layer] = volume;
            _sources[layer].volume = _volume * volume;
        }
        /// <summary>
        /// Set all layer volumes equal to zero
        /// </summary>
        void MuteAll()
        {
            for (int i = 0; i < LayerCount; ++i)
            {
                _layerVolume[i] = 0f;
            }
        }

        /// <summary>
        /// Begin playing the chosen layer
        /// </summary>
        public void Play(params int[] layers)
        {
            MuteAll();

            for (int i = 0; i < layers.Length; ++i)
            {
                if (layers[i] < LayerCount)
                {
                    SetLayerVolume(layers[i], 1f);
                }
            }

        }

        /// <summary>
        /// Fade in selected layers and fade out other layers over default duration
        /// </summary>
        public void FadeToLayers(params int[] layers)
        {
            FadeToLayers(_defaultFadeTime, layers);
        }
        /// <summary>
        /// Fade in selected layers and fade out other layers over specified duration
        /// </summary>
        public void FadeToLayers(float fadetime, params int[] layers)
        {
            int[] fadingOut = new int[LayerCount - layers.Length];

            int index = 0;
            for (int i = 0; i < LayerCount; ++i) //Loop through layers and determine which ones need fading out
            {
                bool isListed = false;
                for (int j = 0; j < layers.Length; ++j)
                {
                    if (i == layers[j])
                    {
                        isListed = true;
                    }
                }

                if (isListed)
                {
                    fadingOut[index++] = i;
                }
            }

            //Pass arrays to fade methods
            FadeInLayers(fadetime, layers);
            FadeOutLayers(fadetime, fadingOut);
        }
        /// <summary>
        /// Fades out selected layers over default duration
        /// </summary>
        public void FadeOutLayers(params int[] layers)
        {
            FadeOutLayers(_defaultFadeTime, layers);
        }
        /// <summary>
        /// Fades out selected layers over specified duration
        /// </summary>
        public void FadeOutLayers(float fadetime, params int[] layers)
        {

        }
        /// <summary>
        /// Fades in selected layers over default duration
        /// </summary>
        public void FadeInLayers(params int[] layers)
        {
            FadeInLayers(_defaultFadeTime, layers);
        }
        /// <summary>
        /// Fades in selected layers over specified duration
        /// </summary>
        public void FadeInLayers(float fadetime, params int[] layers)
        {

        }


        //Coroutines

        */

    }

}