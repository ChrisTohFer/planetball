﻿using UnityEngine;

//Class stores a list of similar sound effects and plays a random one when called

public class RandomSound : MonoBehaviour {
    
    public AudioSource[] Sounds;

    /// <summary>
    /// Multiply volume by fraction (0-1)
    /// </summary>
    public float Volume
    {
        set
        {
            float volume = Mathf.Clamp(value, 0f, 1f);
            volume *= Options.O.EffectVolume;
            foreach (AudioSource a in Sounds)
            {
                a.volume = volume;
            }
        }
    }

    private void OnEnable()
    {
        foreach(AudioSource a in Sounds)
        {
            a.volume = Options.O.EffectVolume;
        }
    }

    public void PlayRandomSound()
    {
        Sounds[Random.Range(0, Sounds.Length)].Play();
    }

}
