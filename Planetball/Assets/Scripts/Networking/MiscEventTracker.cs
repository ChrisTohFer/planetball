﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Bolt;
using Audio;

//Class responds to syncvoice, syncsettings, syncstats events

[BoltGlobalBehaviour]
public class MiscEventTracker : GlobalEventListener {

    //Play synced voice line
    public override void OnEvent(PlayVoice evnt)
    {
        VoicePlayer.PlayLocal((VoiceType)evnt.Type, evnt.SubTrack);
    }

    //Sync game stats
    public override void OnEvent(SyncGameStats evnt)
    {
        GameStatistics.GameStats.RecieveSyncGameStatsEvent(evnt);
    }

    //Sync settings
    public override void OnEvent(SyncSettings evnt)
    {
        GameSettings.RecieveSyncEvent(evnt);
    }

}
