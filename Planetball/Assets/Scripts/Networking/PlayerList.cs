﻿using System.Collections.Generic;
using UnityEngine.Events;
using Bolt;
using UdpKit;

//Class tracks the players in the lobby, wins/losses, blue/red etc

[BoltGlobalBehaviour]
public class PlayerList : GlobalEventListener {

    //Types

    /// <summary>
    /// Class to contain player details
    /// </summary>
    public class Player
    {
        //Variables
        public int Id { get; private set; }
        public string Name { get; private set; }
        public int Wins = 0;
        public int Losses = 0;
        public BoltConnection Connection;

        //Other properties
        public bool IsHost
        {
            get { return Id == _hostId; }
        }
        public bool IsBlue
        {
            get { return Id == _blueId; }
        }
        public bool IsRed
        {
            get { return Id == _redId; }
        }
        public bool IsLocalPlayer
        {
            get { return Id == _localId; }
        }

        //Constructors
        public Player(int id, string name)
        {
            Id = id;
            Name = name;
        }
        public Player(Player player)
        {
            Id = player.Id;
            Name = player.Name;
            Wins = player.Wins;
            Losses = player.Losses;
        }
    }
    /// <summary>
    /// Class for writing and reading playerlist information over the network via bolt events
    /// </summary>
    public class PlayerListToken : IProtocolToken
    {
        public int HostId;
        public int RedId;
        public int BlueId;
        public List<Player> PlayerList;

        public void Read(UdpPacket packet)
        {
            PlayerList = new List<Player>();

            HostId = packet.ReadInt();
            RedId = packet.ReadInt();
            BlueId = packet.ReadInt();

            int count = packet.ReadInt();
            for(int i = 0; i < count; ++i)
            {
                int id = packet.ReadInt();
                string name = packet.ReadString();
                int wins = packet.ReadInt();
                int losses = packet.ReadInt();

                Player player = new Player(id, name);
                player.Wins = wins;
                player.Losses = losses;

                PlayerList.Add(player);
            }
        }
        public void Write(UdpPacket packet)
        {
            packet.WriteInt(HostId);
            packet.WriteInt(RedId);
            packet.WriteInt(BlueId);

            int count = PlayerList.Count;
            packet.WriteInt(count);
            for(int i = 0; i < count; ++i)
            {
                packet.WriteInt(PlayerList[i].Id);
                packet.WriteString(PlayerList[i].Name);
                packet.WriteInt(PlayerList[i].Wins);
                packet.WriteInt(PlayerList[i].Losses);
            }
        }
    }

    //Variables

    public static int MaxPlayers = 8;

    static int _nextId = 10;

    static int _hostId = -1;
    static int _localId = -2;
    static int _redId = -3;
    static int _blueId = -4;

    public static List<Player> Players { get; private set; }

    //Property access

    public static int NextId
    {
        get { return _nextId; }
    }
    public static int LocalId
    {
        get { return _localId; }
        set { _localId = value; }
    }
    public static int HostId
    {
        get { return _hostId; }
        set { _hostId = value; }
    }

    public static int PlayerCount
    {
        get
        {
            if (Players == null) return 0;
            return Players.Count;
        }
    }
    public static Player BluePlayer
    {
        get
        {
            return GetPlayer(_blueId);
        }
    }
    public static Player RedPlayer
    {
        get
        {
            return GetPlayer(_redId);
        }
    }
    public static Player LocalPlayer
    {
        get
        {
            return GetPlayer(_localId);
        }
    }
    public static bool IsLocalPlaying
    {
        get
        {
            return (_localId == _blueId || _localId == _redId);
        }
    }

    //Events
    
    public static UnityEvent OnListUpdated;


    //Initialisation

    /// <summary>
    /// Initialise events
    /// </summary>
    private void Awake()
    {
        OnListUpdated = new UnityEvent();

        Players = new List<Player>();
    }
    /// <summary>
    /// Reset back to initial state
    /// </summary>
    public static void Restart()
    {
        OnListUpdated = new UnityEvent();

        _hostId = -1;
        _localId = -2;
        _redId = -3;
        _blueId = -4;

        Players = new List<Player>();
    }
    /// <summary>
    /// Register PlayerListToken
    /// </summary>
    public override void BoltStartBegin()
    {
        BoltNetwork.RegisterTokenClass<PlayerListToken>();
    }


    //Access methods

    /// <summary>
    /// Return first player object with the given id, returns null if no such player
    /// </summary>
    public static Player GetPlayer(int id)
    {
        Player player = null;

        for(int i = 0; i < PlayerCount; ++i)
        {
            if(Players[i].Id == id)
            {
                player = Players[i];
                break;
            }
        }

        return player;
    }
    /// <summary>
    /// Return the player object associated with the given connection (host only)
    /// </summary>
    public static Player GetPlayer(BoltConnection connection)
    {
        Player player = null;

        for (int i = 0; i < PlayerCount; ++i)
        {
            if (Players[i].Connection == connection)
            {
                player = Players[i];
                break;
            }
        }

        return player;
    }


    //Edit methods

    /// <summary>
    /// Set a new red player
    /// </summary>
    public static void SetRedPlayer(int i)
    {
        _redId = i;
        if (_blueId == i)
        {
            _blueId = -4;
        }

        OnListUpdated.Invoke();
    }
    /// <summary>
    /// Set a new blue player
    /// </summary>
    public static void SetBluePlayer(int i)
    {
        _blueId = i;
        if (_redId == i)
        {
            _redId = -3;
        }

        OnListUpdated.Invoke();
    }
    /// <summary>
    /// Remove player from player slots
    /// </summary>
    public static void SetPlayerSpectator(Player player)
    {
        if (_blueId == player.Id) _blueId = -4;
        if (_redId == player.Id) _redId = -3;

        OnListUpdated.Invoke();
    }
    /// <summary>
    /// Add a new player to the list
    /// </summary>
    public static Player AddNewPlayer(string name, BoltConnection connection = null)
    {
        Player player = new Player(_nextId, name);
        player.Connection = connection;

        Players.Add(player);

        if (PlayerCount == 1) _hostId = player.Id;  //First player added is always host

        OnListUpdated.Invoke();

        ++_nextId;

        return player;
    }
    /// <summary>
    /// Remove player with the given id from the list
    /// </summary>
    public static void RemovePlayer(int id)
    {
        Player player = GetPlayer(id);
        RemovePlayer(player);
    }
    /// <summary>
    /// Remove player with the given boltconnection
    /// </summary>
    public static void RemovePlayer(BoltConnection connection)
    {
        Player player = GetPlayer(connection);
        RemovePlayer(player);
    }
    /// <summary>
    /// Remove player from the list
    /// </summary>
    public static void RemovePlayer(Player player)
    {
        if (player == null)
        {
            UnityEngine.Debug.Log("PlayerList:RemovePlayer; Attempted to remove player that is not in list.");
        }
        else
        {
            if (player.IsBlue)
            {
                _blueId = -4;
            }
            else if (player.IsRed)
            {
                _redId = -3;
            }
            Players.Remove(player);
            OnListUpdated.Invoke();
        }
    }
    /// <summary>
    /// Sets all players to spectators
    /// </summary>
    public static void ClearPlayerSlots()
    {
        _redId = -3;
        _blueId = -4;

        SyncPlayerList();
    }


    //List sharing

    /// <summary>
    /// Send an event to all clients containing the player list information
    /// </summary>
    public static void SyncPlayerList()
    {
        if(BoltNetwork.IsRunning && BoltNetwork.IsServer)
        {
            PlayerListToken token = new PlayerListToken();
            token.HostId = _hostId;
            token.BlueId = _blueId;
            token.RedId = _redId;
            token.PlayerList = Players;

            UpdatePlayerList evnt = UpdatePlayerList.Create();
            evnt.LobbyToken = token;
            evnt.Send();
        }
        else
        {
            UnityEngine.Debug.LogError("Non-server attempted to sync client list");
        }
    }
    /// <summary>
    /// Upon recieving player list event, update details
    /// </summary>
    public override void OnEvent(UpdatePlayerList evnt)
    {
        if(!BoltNetwork.IsServer)
        {
            PlayerListToken token = (PlayerListToken)evnt.LobbyToken;

            _hostId = token.HostId;
            _blueId = token.BlueId;
            _redId = token.RedId;
            Players = token.PlayerList;

            OnListUpdated.Invoke();
        }
    }


    //Position changing

    [System.Serializable]
    /// <summary>
    /// Define positions using enum
    /// </summary>
    public enum Position
    {
        SPECTATOR,
        BLUE,
        RED
    }
    /// <summary>
    /// Request a position change for the local player
    /// </summary>
    public static void RequestChange(Position position)
    {
        if(BoltNetwork.IsServer)
        {
            OnPositionRequest(LocalPlayer, position);
        }
        else
        {
            RequestPositionChange evnt = RequestPositionChange.Create();
            evnt.PositionId = (int)position;
            evnt.Send();
        }
        
    }
    /// <summary>
    /// Recieve requests from players to change between player/spectator slots
    /// </summary>
    public override void OnEvent(RequestPositionChange evnt)
    {
        if(BoltNetwork.IsServer)
        {
            OnPositionRequest(GetPlayer(evnt.RaisedBy), (Position)evnt.PositionId);
        }
    }
    /// <summary>
    /// Handle position change request
    /// </summary>
    static void OnPositionRequest(Player player, Position position)
    {
        if(position == Position.SPECTATOR && (player.IsBlue || player.IsRed))
        {
            SetPlayerSpectator(player);
            SyncPlayerList();
        }
        else if(position == Position.BLUE && BluePlayer == null)
        {
            SetBluePlayer(player.Id);
            SyncPlayerList();
        }
        else if(position == Position.RED && RedPlayer == null)
        {
            SetRedPlayer(player.Id);
            SyncPlayerList();
        }
    }
}
