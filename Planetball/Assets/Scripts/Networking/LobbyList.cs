﻿using System;
using System.Collections.Generic;
using UdpKit;
using Bolt;

//When placed in scene, after startclient called, will periodically refresh list of servers

public class LobbyList : GlobalEventListener {
    
    //Variables
    List<ServerInfoToken> _serverTokens;
    List<UdpSession> _serverUdpSessions;

    //Property access
    public List<ServerInfoToken> ServerTokens
    {
        get {
            if (_serverTokens == null) _serverTokens = new List<ServerInfoToken>();
            return _serverTokens;
        }
    }
    public List<UdpSession> ServerUdpSessions
    {
        get
        {
            if (_serverUdpSessions == null) _serverUdpSessions = new List<UdpSession>();
            return _serverUdpSessions;
        }
    }
    public int ServerCount
    {
        get {
            if (_serverTokens == null) return 0;
            return _serverTokens.Count;
        }
    }

    //Methods

    /// <summary>
    /// Join server with passed in id, or send a notification if the server is no longer available.
    /// </summary>
    /// <returns>
    /// Returns true if join request was sent, or false if server was not available.
    /// </returns>
    public bool JoinServer(Guid serverGuid, string ownName, string password)
    {
        for(int i = 0; i < ServerCount; ++i)
        {
            if(_serverTokens[i].Id == serverGuid)
            {
                ConnectionManager.ConnectToSession(_serverUdpSessions[i], ownName, password);
                return true;
            }
        }

        StaticUIEffects.SendNotification("Chosen server is no longer available.");
        return false;
    }


    //Bolt callbacks

    /// <summary>
    /// Periodically called after startclient; use to update list of servers
    /// </summary>
    public override void SessionListUpdated(Map<Guid, UdpSession> sessionList)
    {
        //Refresh lists
        _serverTokens = new List<ServerInfoToken>();
        _serverUdpSessions = new List<UdpSession>();

        foreach(var session in sessionList)
        {
            //Get token and check type
            IProtocolToken token = session.Value.GetProtocolToken();
            if(token.GetType() == typeof(ServerInfoToken))        //Possibly needs to be in try in case of null token?
            {

                //If correct version, add to list
                ServerInfoToken lobbyToken = (ServerInfoToken)token;
                if(lobbyToken.IsCorrectVersion)
                {
                    _serverTokens.Add(lobbyToken);
                    _serverUdpSessions.Add(session.Value);
                }

            }

        }

        UnityEngine.Debug.Log("Servers detected: " + _serverTokens.Count);
    }

}
