﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;

//Class responsible for setting the values for player badges in the lobby

public class PlayerBadge : MonoBehaviour
{

    //References

    [Header("Component references")]
    [SerializeField] TextMeshProUGUI _nameField;
    [SerializeField] TextMeshProUGUI _winLossField;
    [SerializeField] Image _background;
    [SerializeField] Image _playerIcon;

    //Image references

    [Header("Image references")]
    [SerializeField] Sprite _filledSprite;
    [SerializeField] Sprite _emptySprite;
    [SerializeField] Sprite _brightPlayerIcon;
    [SerializeField] Sprite _darkPlayerIcon;

    //Text colours

    [Header("Text colours")]
    [SerializeField] Color _brightColour;
    [SerializeField] Color _darkColour;

    //Variables

    [SerializeField] PlayerList.Position _position;
    bool _filled = false;

    //Events

    public UnityEvent SlotFilled;
    public UnityEvent SlotEmptied;


    //Initialisation
    

    //Methods

    /// <summary>
    /// Set the namefield of the badge
    /// </summary>
    void SetName(string name)
    {
        _nameField.text = name;
    }
    /// <summary>
    /// Set the win/loss text field value of the badge
    /// </summary>
    void SetWinLoss(int wins, int losses)
    {
        string text = wins + "W/" + losses + "L";
        _winLossField.text = text;
    }

    /// <summary>
    /// Set the badge as occupied and fill in details
    /// </summary>
    public void SetOccupied(string name, int wins, int losses)
    {
        SetName(name);
        SetWinLoss(wins, losses);

        _nameField.color = _darkColour;
        _winLossField.color = _darkColour;

        _playerIcon.sprite = _darkPlayerIcon;
        _background.sprite = _filledSprite;

        _filled = true;

        SlotFilled.Invoke();
    }
    /// <summary>
    /// Call SetOccupied and provide with player details
    /// </summary>
    public void SetOccupied(PlayerList.Player player)
    {
        SetOccupied(player.Name, player.Wins, player.Losses);
    }
    /// <summary>
    /// Set bade as unoccupied and remove details
    /// </summary>
    public void SetEmpty()
    {
        _nameField.text = "Empty";
        _nameField.color = _brightColour;

        _winLossField.text = "";
        _winLossField.color = _brightColour;

        _playerIcon.sprite = _brightPlayerIcon;
        _background.sprite = _emptySprite;

        _filled = false;

        SlotEmptied.Invoke();
    }

    /// <summary>
    /// Attempt to fill the slot with the local player
    /// </summary>
    public void LocalFillSlot()
    {
        if(!_filled)
        {
            PlayerList.RequestChange(_position);
        }
    }


    /*
    [SerializeField] TextMeshProUGUI _nameTextObject;
    [SerializeField] TextMeshProUGUI _winLossTextObject;
    [SerializeField] Image _background;
    [SerializeField] Image _hostIcon;


    int _playerId;

    /// <summary>
    /// Sets the badge to an "empty" state
    /// </summary>
    public void NullAppearance()
    {
        _playerId = -1;
        UpdateAppearance();
    }
    /// <summary>
    /// Update appearance based on the id passed in
    /// </summary>
    public void UpdateAppearance(int id)
    {
        _playerId = id;
        UpdateAppearance();
    }
    /// <summary>
    /// Update appearance based on stored id
    /// </summary>
    public void UpdateAppearance()
    {
        LobbyControl.Player player = LobbyControl.GetPlayer(_playerId);

        if(player != null)
        {
            _nameTextObject.text = player.Name;
            if (player.Id == LobbyControl.LocalId)
            {
                _nameTextObject.color = Color.green;
            }
            else
            {
                _nameTextObject.color = Color.white;
            }

            _winLossTextObject.text = player.Wins + "W/" + player.Losses + "L";

            _hostIcon.enabled = player.IsHost;
        }
        else
        {
            _nameTextObject.text = "Empty";
            _nameTextObject.color = Color.white;
            _winLossTextObject.text = "W/L";
            _hostIcon.enabled = false;
        }
    }
    */
}
