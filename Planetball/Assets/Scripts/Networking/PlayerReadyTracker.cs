﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Bolt;
using UdpKit;
using UnityEngine.Events;

//Class keeps track of when players are ready to play

[BoltGlobalBehaviour]
public class PlayerReadyTracker : GlobalEventListener {

    //Types

    /// <summary>
    /// Token to send information about changes in the ready state of players
    /// </summary>
    public class ReadyToken : IProtocolToken
    {
        public bool BlueReady = false;
        public bool BlueUnready = false;
        public bool RedReady = false;
        public bool RedUnready = false;

        public void Read(UdpPacket packet)
        {
            BlueReady = packet.ReadBool();
            BlueUnready = packet.ReadBool();
            RedReady = packet.ReadBool();
            RedUnready = packet.ReadBool();
        }
        public void Write(UdpPacket packet)
        {
            packet.WriteBool(BlueReady);
            packet.WriteBool(BlueUnready);
            packet.WriteBool(RedReady);
            packet.WriteBool(RedUnready);
        }
    }


    //Variables

    static bool _blueReady = false;
    static bool _redReady = false;

    public static bool BlueReady
    {
        get { return _blueReady; }
    }
    public static bool RedReady
    {
        get { return _redReady; }
    }

    //Events

    public static UnityEvent OnReadyChanged;

    //Initialisation

    /// <summary>
    /// Initialise events
    /// </summary>
    private void Awake()
    {
        OnReadyChanged = new UnityEvent();
    }


    //Readiness setting methods

    public static void SetBlueReady(bool isReady)
    {
        ReadyToken token = new ReadyToken
        {
            BlueReady = (isReady),
            BlueUnready = (!isReady)
        };

        MiscTokenEvent evnt = MiscTokenEvent.Create();
        evnt.Token = token;
        evnt.Send();
    }
    public static void SetRedReady(bool isReady)
    {
        ReadyToken token = new ReadyToken
        {
            RedReady = (isReady),
            RedUnready = (!isReady)
        };

        MiscTokenEvent evnt = MiscTokenEvent.Create();
        evnt.Token = token;
        evnt.Send();
    }
    public static void CancelReady()
    {
        ReadyToken token = new ReadyToken
        {
            BlueUnready = true,
            RedUnready = true
        };

        MiscTokenEvent evnt = MiscTokenEvent.Create();
        evnt.Token = token;
        evnt.Send();
    }

    public static void SyncReadiness()
    {
        ReadyToken token = new ReadyToken {
            BlueReady = _blueReady,
            BlueUnready = !_blueReady,
            RedReady = _redReady,
            RedUnready = !_redReady
        };

        MiscTokenEvent evnt = MiscTokenEvent.Create();
        evnt.Token = token;
        evnt.Send();
    }

    //Callbacks

    /// <summary>
    /// Handle ReadyToken events
    /// </summary>
    public override void OnEvent(MiscTokenEvent evnt)
    {
        if (evnt.Token.GetType() == typeof(ReadyToken))
        {
            ReadyToken token = (ReadyToken)evnt.Token;

            if (token.BlueReady)
            {
                _blueReady = true;
            }
            else if (token.BlueUnready)
            {
                _blueReady = false;
            }

            if (token.RedReady)
            {
                _redReady= true;
            }
            else if (token.RedUnready)
            {
                _redReady = false;
            }

            OnReadyChanged.Invoke();
        }
    }

    /// <summary>
    /// Register ready token on initialisation of bolt
    /// </summary>
    public override void BoltStartBegin()
    {
        BoltNetwork.RegisterTokenClass<ReadyToken>();
    }

}
