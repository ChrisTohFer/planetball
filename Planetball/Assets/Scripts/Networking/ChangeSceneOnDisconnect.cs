﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeSceneOnDisconnect : MonoBehaviour {

    //Editor values

    [SerializeField] string _sceneName;
    [SerializeField] string _message = "Lost connection. Click to return to menu.";

    //Private vars

    bool _isChangingScene = false;

    //Update

    /// <summary>
    /// Check if still bolt is still running, send message if not
    /// </summary>
    private void FixedUpdate()
    {
        if(!_isChangingScene && !BoltNetwork.IsRunning)
        {
            _isChangingScene = true;
            StaticUIEffects.SendNotification(_message, methodOnDismissal: LoadScene);
        }
    }

    //Methods

    /// <summary>
    /// Use to load new scene upon bolt failure
    /// </summary>
    void LoadScene()
    {
        SceneManager.LoadScene(_sceneName);
    }

}
