﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Bolt;
using UdpKit;

//Class for handing connections

[BoltGlobalBehaviour]
public class ConnectionManager : GlobalEventListener {

    //Types

    /// <summary>
    /// Token to provide information to host when a client attempts to connect
    /// </summary>
    public class ConnectRequestToken : IProtocolToken
    {
        public string PlayerName;
        public string Password;
        public string Version;

        public void Read(UdpPacket packet)
        {
            PlayerName = packet.ReadString();
            Password = packet.ReadString();
            Version = packet.ReadString();
        }
        public void Write(UdpPacket packet)
        {
            packet.WriteString(PlayerName);
            packet.WriteString(Password);
            packet.WriteString(StaticInfo.Version);
        }
    }
    /// <summary>
    /// Token to provide information to client on acceptance
    /// </summary>
    public class ConnectAcceptToken : IProtocolToken
    {
        public int Id;

        public void Read(UdpPacket packet)
        {
            Id = packet.ReadInt();
        }
        public void Write(UdpPacket packet)
        {
            packet.WriteInt(Id);
        }
    }


    //Static singleton

    static ConnectionManager _singleton;

    //Variables

    public static string ServerName = "";
    public static string HostName = "";
    public static string Password = "";

    //Server token
    static ServerInfoToken _serverToken;


    //Initialisation

    /// <summary>
    /// Set _singleton to this object if null
    /// </summary>
    private void Awake()
    {
        if(_singleton == null)
        {
            _singleton = this;
        }
    }
    /// <summary>
    /// Destroy this object if already have a connection manager
    /// </summary>
    private void Start()
    {
        if(_singleton != this)
        {
            Destroy(gameObject);
        }
    }
    /// <summary>
    /// Register tokens
    /// </summary>
    public override void BoltStartBegin()
    {
        BoltNetwork.RegisterTokenClass<ConnectRequestToken>();
        BoltNetwork.RegisterTokenClass<ConnectAcceptToken>();
    }


    //Server methods

    /// <summary>
    /// Updates information on the server token that may have changed
    /// </summary>
    public static void UpdateServerToken()
    {
        _serverToken.Players = PlayerList.PlayerCount;
        _serverToken.MaxPlayers = PlayerList.MaxPlayers;

        BoltNetwork.SetServerInfo(_serverToken.Id.ToString(), _serverToken);
    }
    /// <summary>
    /// Set up initial server info
    /// </summary>
    public override void BoltStartDone()
    {
        if(BoltNetwork.IsServer)
        {
            PlayerList.Player host = PlayerList.AddNewPlayer(HostName);
            PlayerList.HostId = host.Id;
            PlayerList.LocalId = host.Id;

            _serverToken = new ServerInfoToken();

            _serverToken.Version = StaticInfo.Version;
            _serverToken.HostName = HostName;
            _serverToken.Name = ServerName;
            _serverToken.Players = 1;
            _serverToken.MaxPlayers = PlayerList.MaxPlayers;
            _serverToken.RequiresPassword = (Password != "");

            BoltNetwork.SetServerInfo(_serverToken.Id.ToString(), _serverToken);

            BoltNetwork.LoadScene("Lobby");
        }
    }

    //Client methods

    /// <summary>
    /// Attempt to connect to a given session
    /// </summary>
    public static void ConnectToSession(UdpSession session, string name, string password = "")
    {
        ConnectRequestToken requestToken = new ConnectRequestToken();
        requestToken.PlayerName = name;
        requestToken.Password = password;
        requestToken.Version = StaticInfo.Version;

        ServerInfoToken serverToken = (ServerInfoToken)session.GetProtocolToken();
        ServerName = serverToken.Name;

        BoltNetwork.Connect(session, requestToken);
    }
    

    //Connection events

    /// <summary>
    /// On connect attempt, check if room for new player and if password is correct
    /// </summary>
    public override void ConnectRequest(UdpEndPoint endpoint, IProtocolToken token)
    {
        ConnectRequestToken requestToken = (ConnectRequestToken)token;

        if(BoltNetwork.IsServer)
        {

            if (requestToken.Version != StaticInfo.Version)
            {
                Debug.Log("Connect attempt rejected; version");
                MessageToken messageToken = new MessageToken { Message = "Connect attempt refused; incorrect game version." };
                BoltNetwork.Refuse(endpoint, messageToken);
            }
            else if(PlayerList.PlayerCount >= PlayerList.MaxPlayers)
            {
                Debug.Log("Connect attempt rejected; full");
                MessageToken messageToken = new MessageToken { Message = "Connect attempt refused; server is full." };
                BoltNetwork.Refuse(endpoint, messageToken);
            }
            else if(Password != "" && requestToken.Password != Password)
            {
                Debug.Log("Connect attempt rejected; password");
                MessageToken messageToken = new MessageToken { Message = "Connect attempt refused; incorrect password." };
                BoltNetwork.Refuse(endpoint, messageToken);
            }
            else
            {
                Debug.Log("Connect attempt accepted");
                ConnectAcceptToken acceptToken = new ConnectAcceptToken { Id = PlayerList.NextId };
                BoltNetwork.Accept(endpoint, acceptToken);
            }
        }
    }
    /// <summary>
    /// On successful connection, host adds player to player list and sends out update to all players
    /// Client sets its own id locally
    /// </summary>
    public override void Connected(BoltConnection connection)
    {
        if(BoltNetwork.IsServer)
        {
            ConnectRequestToken requestToken = (ConnectRequestToken)connection.ConnectToken;
            PlayerList.Player player = PlayerList.AddNewPlayer(requestToken.PlayerName, connection);

            UpdateServerToken();
            PlayerList.SyncPlayerList();
            GameSettings.SendSettingsToClient();
        }
        else
        {
            ConnectAcceptToken acceptToken = (ConnectAcceptToken)connection.AcceptToken;
            PlayerList.LocalId = acceptToken.Id;
        }
    }
    /// <summary>
    /// Display message on screen if connection attempt failed
    /// </summary>
    public override void ConnectRefused(UdpEndPoint endpoint, IProtocolToken token)
    {
        if(!BoltNetwork.IsServer)
        {
            MessageToken refusalToken = (MessageToken)token;
            StaticUIEffects.SendNotification(refusalToken.Message);
        }
    }


    //Disconnect events

    /// <summary>
    /// On disconnect, remove player from playerlist
    /// </summary>
    public override void Disconnected(BoltConnection connection)
    {
        if (BoltNetwork.IsServer)
        {
            PlayerList.RemovePlayer(connection);

            UpdateServerToken();
            PlayerList.SyncPlayerList();
        }
    }
}
