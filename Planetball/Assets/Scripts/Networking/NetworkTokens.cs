﻿using System.Collections;
using System.Collections.Generic;
using UdpKit;
using UnityEngine;

//Define and initialise some core tokens for use when connecting to lobbies

/// <summary>
/// Token is used to obtain information about a server prior to connecting to it
/// </summary>
public class ServerInfoToken : Bolt.IProtocolToken
{
    public string Version;
    public System.Guid Id = System.Guid.NewGuid();
    public string Name;
    public string HostName;
    public int Players;
    public int MaxPlayers;
    public bool RequiresPassword;

    public bool IsCorrectVersion
    {
        get { return Version == StaticInfo.Version; }
    }
    public bool IsFull
    {
        get { return Players == MaxPlayers; }
    }

    public void Read(UdpPacket packet)
    {
        Version = packet.ReadString();

        if (IsCorrectVersion)    //Only read remainder of token if version is correct, since the other data may have changed
        {
            Id = packet.ReadGuid();
            Name = packet.ReadString();
            HostName = packet.ReadString();
            Players = packet.ReadInt();
            MaxPlayers = packet.ReadInt();
            RequiresPassword = packet.ReadBool();
        }
    }
    public void Write(UdpPacket packet)
    {
        packet.WriteString(Version);
        packet.WriteGuid(Id);
        packet.WriteString(Name);
        packet.WriteString(HostName);
        packet.WriteInt(Players);
        packet.WriteInt(MaxPlayers);
        packet.WriteBool(RequiresPassword);
    }
}

/// <summary>
/// Token provides a means of sending simple string messages on disconnect etc
/// </summary>
public class MessageToken : Bolt.IProtocolToken
{
    public string Message;

    public void Read(UdpPacket packet)
    {
        Message = packet.ReadString();
    }
    public void Write(UdpPacket packet)
    {
        packet.WriteString(Message);
    }
}

/// <summary>
/// Register previously defined tokens
/// </summary>
[BoltGlobalBehaviour]
public class TokenRegistration : Bolt.GlobalEventListener
{
    /// <summary>
    /// Register tokens with boltnetwork
    /// </summary>
    public override void BoltStartBegin()
    {
        BoltNetwork.RegisterTokenClass<ServerInfoToken>();
        BoltNetwork.RegisterTokenClass<MessageToken>();
        
        BoltNetwork.RegisterTokenClass<GameSettings.SettingsToken>();

        BoltNetwork.RegisterTokenClass<GameSceneControl.GameFlagsToken>();

        BoltNetwork.RegisterTokenClass<GameStatistics.GameStats.GameStatsToken>();
    }

}