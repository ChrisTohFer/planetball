﻿using UnityEngine;

using System.Collections.Generic;
using System.IO;

public class Controls : MonoBehaviour {

    //Self reference
    static Controls _controls;

    //Arrays to store keycodes and tags
    [SerializeField] string[] _tags;
    [SerializeField] KeyCode[] _defaults;
    [SerializeField] Dictionary<string, KeyCode> _keys;


    //Initialisation

    /// <summary>
    /// Set up singleton stuff, load values from file
    /// </summary>
    private void Awake()
    {
        if(_controls == null)
        {
            DontDestroyOnLoad(gameObject);
            _controls = this;
            
            Load();
        }
        else
        {
            Destroy(gameObject);
        }
    }


    //Save/load

    /// <summary>
    /// Save current values to the file
    /// </summary>
    public static void Save()
    {
        FileInfo file = _controls.GetFile();
        file.Delete();  //Ensure file is empty

        BinaryWriter output = new BinaryWriter(file.OpenWrite());

        for(int i = 0; i < _controls._tags.Length; ++i)
        {
            output.Write((int)_controls._keys[_controls._tags[i]]);
        }
        
        output.Close();
    }
    /// <summary>
    /// Overwrite current values with those from the file
    /// </summary>
    public static void Load()
    {
        FileInfo file = _controls.GetFile();

        if(!file.Exists)
        {
            LoadDefaults();
            Save();
            return;
        }

        BinaryReader input = new BinaryReader(file.OpenRead());
        _controls._keys = new Dictionary<string, KeyCode>();

        try
        {
            for (int i = 0; i < _controls._tags.Length; ++i)
            {
                _controls._keys.Add(_controls._tags[i], (KeyCode)input.ReadInt32());
            }
        }
        catch
        {
            LoadDefaults();
            Save();
        }

        input.Close();
    }
    /// <summary>
    /// Overwrite current values with the defaults
    /// </summary>
    public static void LoadDefaults()
    {
        _controls._keys = new Dictionary<string, KeyCode>();
        for(int i = 0; i < _controls._tags.Length; ++i)
        {
            _controls._keys.Add(_controls._tags[i], _controls._defaults[i]);
        }
    }

    /// <summary>
    /// Returns the FileInfo for controls.dat
    /// </summary>
    private FileInfo GetFile()
    {
        DirectoryInfo home = new DirectoryInfo(".");
        DirectoryInfo config = new DirectoryInfo(home.FullName + "/config");
        if(!config.Exists)
        {
            config.Create();
        }

        FileInfo file = new FileInfo(config.FullName + "/controls.dat");

        return file;
    }


    //Access methods

    /// <summary>
    /// Get the keycode for an action with the specified name
    /// </summary>
    public static KeyCode GetKey(string name)
    {
        return _controls._keys[name];
    }
    /// <summary>
    /// Set the keycode for an action with the specified name
    /// </summary>
    public static void SetKey(string name, KeyCode value)
    {
        _controls._keys[name] = value;
    }

}
