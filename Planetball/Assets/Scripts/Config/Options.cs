﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class Options : MonoBehaviour {

    //Global static reference
    public static Options O;

    //Values
    public float MasterVolume = 1f;
    public float MusicVolume = 1f;
    public float VoiceVolume = 1f;
    public float EffectVolume = 1f;
    public float ScreamVolume = 0.1f;

    private void Awake()
    {
        O = this;
        LoadFromFile();
    }
    private void Start()
    {
        SetMasterVolume(MasterVolume);
        SetMusicVolume(MusicVolume);
        SetVoiceVolume(VoiceVolume);
    }

    public void SetMasterVolume(float vol)
    {
        MasterVolume = vol;
        AudioListener.volume = MasterVolume;
    }
    public void SetMusicVolume(float vol)
    {
        MusicVolume = vol;
        Audio.MusicPlayer.Volume = vol;
    }
    public void SetVoiceVolume(float vol)
    {
        VoiceVolume = vol;
        Audio.VoicePlayer.Volume = vol;
    }
    public void SetEffectsVolume(float vol)
    {
        EffectVolume = vol;
    }
    public void SetScreamVolume(float vol)
    {
        ScreamVolume = vol;
    }
    public void SetMasterVolume(Slider slider)
    {
        SetMasterVolume(slider.value);
    }
    public void SetMusicVolume(Slider slider)
    {
        SetMusicVolume(slider.value);
    }
    public void SetVoiceVolume(Slider slider)
    {
        SetVoiceVolume(slider.value);
    }
    public void SetEffectsVolume(Slider slider)
    {
        EffectVolume = slider.value;
    }
    public void SetScreamVolume(Slider slider)
    {
        ScreamVolume = slider.value;
    }

    public void SaveToFile()
    {
        FileInfo file = GetFile();
        file.Delete();

        BinaryWriter write = new BinaryWriter(file.OpenWrite());
        write.Write(MasterVolume);
        write.Write(MusicVolume);
        write.Write(VoiceVolume);
        write.Write(EffectVolume);
        write.Write(ScreamVolume);

        write.Close();
    }
    private void LoadFromFile()
    {
        FileInfo file = GetFile();

        if(file.Exists)
        {
            BinaryReader read = new BinaryReader(file.OpenRead());

            try
            {
                float Master = read.ReadSingle();
                float Music = read.ReadSingle();
                float Voice = read.ReadSingle();
                float Effect = read.ReadSingle();
                float Scream = read.ReadSingle();

                MasterVolume = Master;
                MusicVolume = Music;
                VoiceVolume = Voice;
                EffectVolume = Effect;
                ScreamVolume = Scream;
            }
            catch
            {
                Debug.Log("Failed to load options from file");
            }

            read.Close();
        }
    }
    private FileInfo GetFile()
    {
        DirectoryInfo local = new DirectoryInfo(".");
        DirectoryInfo config = new DirectoryInfo(local.FullName + "/config");

        if (!config.Exists)
        {
            config.Create();
        }

        return new FileInfo(config.FullName + "/Options.dat");
    }

}
