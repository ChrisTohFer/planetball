﻿using UnityEngine;
using System.IO;
using UdpKit;
using UnityEngine.Events;

public class GameSettings : MonoBehaviour {

    //Global reference
    static GameSettings _singleton;

    //Types

    ///Class to group setting value with min/max/default values etc
    [System.Serializable]
    public class FloatSetting
    {
        [SerializeField] Type id;
        public Type Id
        {
            get { return id; }
        }
        [SerializeField] float _min;
        public float Min
        {
            get { return _min; }
        }
        [SerializeField] float _max;
        public float Max
        {
            get { return _max; }
        }
        [SerializeField] float _default;
        public float Default
        {
            get { return _default; }
        }

        float _value;
        public float Value
        {
            get { return Mathf.Clamp(_value, _min, _max); }
            set { _value = Mathf.Clamp(value, _min, _max); }
        }
        public int ValueInt
        {
            get { return Mathf.RoundToInt(Value); }
        }

        public void SetDefault()
        {
            Value = _default;
        }
    }

    ///Enum to identify setting type
    public enum Type
    {
        PLANET_COOLDOWN,
        PLANET_SPAWNSPEED,
        PLANET_MINSIZE,
        PLANET_MAXSIZE,
        PLANET_MAXCOUNT,
        PLAYER_SPEED,
        PLAYER_HEALTH,
        PLAYER_GRAV_ACCEL,
        PLAYER_GRAV_TOPSPEED
    }

    ///Token to sync settings via events
    public class SettingsToken : Bolt.IProtocolToken
    {
        public FloatSetting[] Settings;
        public float[] Values;

        public void Read(UdpPacket packet)
        {
            int count = packet.ReadInt();

            Values = new float[count];

            for(int i = 0; i < count; ++i)
            {
                Values[i] = packet.ReadFloat();
            }
        }
        public void Write(UdpPacket packet)
        {
            packet.WriteInt(Settings.Length);

            for(int i = 0; i < Settings.Length; ++i)
            {
                packet.WriteFloat(Settings[i].Value);
            }
        }
    }
    
    //Settings values

    public FloatSetting[] _settings;

    //Events
    public static UnityEvent OnSync;


    //Initialisation

    /// <summary>
    /// Set up singleton and load from file
    /// </summary>
    private void Awake()
    {
        if(_singleton == null)
        {
            DontDestroyOnLoad(gameObject);
            _singleton = this;
            OnSync = new UnityEvent();

            LoadFromFile();
        }
    }
    /// <summary>
    /// Destroy if duplicate settings manager
    /// </summary>
    private void Start()
    {
        if(_singleton != this)
        {
            Destroy(gameObject);
        }
    }


    //Defaults
    public static void SetAllDefaults()
    {
        foreach(FloatSetting fs in _singleton._settings)
        {
            fs.SetDefault();
        }
    }

    //Return the setting with the specified name;
    public static FloatSetting GetSetting(Type id)
    {
        FloatSetting val = null;

        foreach(FloatSetting fs in _singleton._settings)
        {
            if(fs.Id == id)
            {
                val = fs;
                return val;
            }
        }

        return val;
    }

    //Get the file to write to/read from
    private static FileInfo GetFile()
    {
        DirectoryInfo workingDir = new DirectoryInfo(".");
        DirectoryInfo configDir = new DirectoryInfo(workingDir + "/config");

        if(!configDir.Exists)
        {
            configDir.Create();
        }

        return new FileInfo(configDir.FullName + "/gameSettings.dat");
    }

    //Delete old file and write in new values
    public static void SaveToFile()
    {
        FileInfo file = GetFile();
        file.Delete();

        BinaryWriter writer = new BinaryWriter(file.Create());

        for(int i = 0; i < _singleton._settings.Length; ++i)
        {
            writer.Write(_singleton._settings[i].Value);
        }

        writer.Close();
    }
    //Read from file
    public static void LoadFromFile()
    {
        try
        {
            FileInfo file = GetFile();

            if(file.Exists)
            {
                BinaryReader reader = new BinaryReader(file.OpenRead());

                for (int i = 0; i < _singleton._settings.Length; ++i)
                {
                    _singleton._settings[i].Value = reader.ReadSingle();
                }

                reader.Close();
            }
            else
            {
                SetAllDefaults();
            }
        }
        catch
        {
            Debug.Log("Failed to load settings from file");
            SetAllDefaults();
        }
    }

    /// <summary>
    /// Sends a global event for clients to sync their game settings; ONLY CALL WHEN BOLT ACTIVE
    /// </summary>
    public static void SendSettingsToClient()
    {
        SyncSettings evnt = SyncSettings.Create();

        SettingsToken st = new SettingsToken();
        st.Settings = _singleton._settings;

        evnt.SettingsToken = st;
        evnt.Send();
    }

    /// <summary>
    /// Copy over game settings from syncevent
    /// </summary>
    public static void RecieveSyncEvent(SyncSettings evnt)
    {
        if(!BoltNetwork.IsServer)
        {
            SettingsToken st = (SettingsToken)evnt.SettingsToken;

            for (int i = 0; i < _singleton._settings.Length; ++i)
            {
                _singleton._settings[i].Value = st.Values[i];
            }

            OnSync.Invoke();
        }
    }
}
