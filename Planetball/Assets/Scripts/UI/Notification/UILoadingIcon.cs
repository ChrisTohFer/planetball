﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILoadingIcon : MonoBehaviour {

    [SerializeField] RectTransform _transform;
    [SerializeField] float _spinSpeed;

    private void FixedUpdate()
    {
        _transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, _transform.rotation.eulerAngles.z + _spinSpeed * Time.fixedDeltaTime));
    }

}
