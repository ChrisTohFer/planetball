﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MenuUI
{
    //Class is simply used to dismiss notifications; can run a delegate method on dismissal
    public class UINotification : MonoBehaviour
    {
        [SerializeField] GameObject _notificationCanvas;

        public delegate void RunOnDismissal();
        public RunOnDismissal dismissalMethod;

        public void OnClick()
        {
            if(dismissalMethod != null)
            {
                dismissalMethod.Invoke();
            }

            Destroy(_notificationCanvas);
        }

    }
}
