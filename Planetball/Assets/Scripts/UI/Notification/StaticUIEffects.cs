﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

//Class provides a global means of sending a notification to a player or creating loading symbols, etc

public class StaticUIEffects : MonoBehaviour {

    //Global reference
	public static StaticUIEffects _singleton;
    
    //Resource references
    [SerializeField] GameObject _messagePrefab;
    [SerializeField] GameObject _loadIconPrefab;

    //Private variables
    GameObject _loadIcon;
    int _messagePriority = 10;

    public static bool LoadIconPresent
    {
        get { return _singleton._loadIcon != null; }
    }


    //Initialisation

    private void Awake()
    {
        _singleton = this;
    }


    //Methods to create and disable the loading icon

    /// <summary>
    /// Create a loading icon in the corner of the screen if not already present
    /// </summary>
    public static void StartLoadIcon()
    {
        if(!LoadIconPresent)
        {
            _singleton._loadIcon = Instantiate(_singleton._loadIconPrefab);
        }
    }
    /// <summary>
    /// Remove loading icon from the corner of the screen if present
    /// </summary>
    public static void StopLoadIcon()
    {
        if(LoadIconPresent)
        {
            Destroy(_singleton._loadIcon);
        }
    }


    //Notifications

    /// <summary>
    /// Create a notification on screen which can be dismissed by clicking
    /// </summary>
    public static void SendNotification(string message, int boostedPriority = 0, System.Action methodOnDismissal = null)
    {
        GameObject o = Instantiate(_singleton._messagePrefab);

        Canvas c = o.GetComponent<Canvas>();
        c.sortingOrder = _singleton._messagePriority + boostedPriority;

        _singleton._messagePriority++; //Boost priority so that later messages appear on top

        TextMeshProUGUI text = c.GetComponentInChildren<TextMeshProUGUI>();
        text.text = message;

        if(methodOnDismissal != null)
        {
            MenuUI.UINotification notificationScript = c.GetComponentInChildren<MenuUI.UINotification>();
            notificationScript.dismissalMethod = new MenuUI.UINotification.RunOnDismissal(methodOnDismissal);
        }
    }
}
