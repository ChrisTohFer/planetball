﻿using UnityEngine;
using TMPro;
public class FontChange : MonoBehaviour {

    public TextMeshProUGUI ButtonText;
    public Material BaseText;
    public Material SelectedText;

    public void SelectedFont()
    {
        ButtonText.fontSharedMaterial = SelectedText;
    }
    public void DeselectedFont()
    {
        ButtonText.fontSharedMaterial = BaseText;
        
    }
}
