﻿using System.Collections;
using UnityEngine.Events;
using UnityEngine;

public class UISlideInOut : MonoBehaviour {

    //Entrance properties

    [Header("Entrance")]
    [SerializeField] bool _enterOnEnable = true;
    [SerializeField] float _enterTravelDistance = 0f;
    [SerializeField] float _enterDuration = 0f;
    [SerializeField] float _enterBaseSpeed = 3f;
    [Tooltip("UI element will slide in from this direction (will be normalised)")][SerializeField] Vector3 _travelFrom;
    [SerializeField] float _enterDelay = 0f;

    float _enterDynamicSpeed;

    //Exit properties

    [Header("Exit")]
    [SerializeField] bool _disableOnExit = true;
    [SerializeField] float _exitTravelDistance = 0f;
    [SerializeField] float _exitDuration = 0f;
    [SerializeField] float _exitBaseSpeed = 3f;
    [Tooltip("UI element will slide out in this direction (will be normalised)")] [SerializeField] Vector3 _travelTo;
    [SerializeField] float _exitDelay = 0f;

    float _exitDynamicSpeed;

    //References

    [Header("References")]
    [SerializeField] UISlideInOut[] _subComponents;
    [SerializeField] ControlInteractive[] _interactive;

    //Variables
    Vector3 _origin;
    bool _initialised = false;
    bool _hasOwner = false;
    RectTransform _rectTransform;
    bool _animating = false;
    bool _queueExit = false;

    //Events
    /// <summary>
    /// Called when element has finished entering
    /// </summary>
    public UnityEvent EntranceFinished;

    /// <summary>
    /// Called when element has finished exiting. Listeners are reset after invoking.
    /// </summary>
    public UnityEvent ExitFinished;

    //Property access

    public float EntranceLength
    {
        get { return _enterDelay + _enterDuration; }
    }
    public float ExitLength
    {
        get { return _exitDelay + _exitDuration; }
    }
    public bool Animating
    {
        get { return _animating; }
    }

    //Initialisation

    /// <summary>
    /// On first enable, initialise
    /// Activate entrance if _enteronenable is true
    /// </summary>
    private void OnEnable()
    {
        Initialize();
        _queueExit = false;

        if(_enterOnEnable)
        {
            Enter();
        }
    }
    private void OnDisable()
    {
        _animating = false;
    }

    /// <summary>
    /// Set origin position and rect transform reference
    /// Calculate/set parameters
    /// Initialise sub components
    /// </summary>
    private void Initialize(bool external = false)
    {
        if(external)
        {
            _hasOwner = true;
        }

        if (!_initialised)
        {
            _rectTransform = GetComponent<RectTransform>();
            _origin = _rectTransform.anchoredPosition;

            //Set enable/disable bools in sub components to false
            foreach (UISlideInOut component in _subComponents)
            {
                component._enterOnEnable = false;
                component._disableOnExit = false;
                component.Initialize(true);
            }

            //Set entrance variables
            if (_enterDuration > 0f)
            {
                _enterBaseSpeed = Mathf.Min(_enterBaseSpeed, _enterTravelDistance / _enterDuration);
                _enterDynamicSpeed = (2f / _enterDuration) * (_enterTravelDistance - _enterBaseSpeed * _enterDuration);
            }
            else
            {
                _enterBaseSpeed = 0f;
                _enterDynamicSpeed = 0f;
                _enterTravelDistance = 0f;
                _enterDelay = 0f;

                //If duration is null, set to total duration of longest subcomponent
                foreach (UISlideInOut component in _subComponents)
                {
                    if (component.EntranceLength > _enterDelay) _enterDelay = component.EntranceLength;
                }
            }

            //Set exit variables
            if (_exitDuration > 0f)
            {
                _exitBaseSpeed = Mathf.Min(_exitBaseSpeed, _exitTravelDistance / _exitDuration);
                _exitDynamicSpeed = (2f / _exitDuration) * (_exitTravelDistance - _exitBaseSpeed * _exitDuration);
            }
            else
            {
                _exitBaseSpeed = 0f;
                _exitDynamicSpeed = 0f;
                _exitTravelDistance = 0f;

                //If duration is null, set to total duration of longest subcomponent
                foreach (UISlideInOut component in _subComponents)
                {
                    if (component.ExitLength > _exitDelay) _exitDelay = component.ExitLength;
                }
            }

            _initialised = true;
        }
    }


    //Enter and exit methods

    /// <summary>
    /// UI element enters the screen
    /// </summary>
    public void Enter()
    {
        Initialize(false);

        if (!gameObject.activeSelf)
        {
            gameObject.SetActive(true);
        }

        foreach(UISlideInOut component in _subComponents)
        {
            component.Enter();
        }

        StopAllCoroutines();
        StartCoroutine(CEnter());
    }
    /// <summary>
    /// UI element exits the screen
    /// </summary>
    public void Exit()
    {
        foreach (UISlideInOut component in _subComponents)
        {
            component.Exit();
        }

        if(gameObject.activeInHierarchy)
        {
            StopAllCoroutines();
            StartCoroutine(CExit());
        }
    }
    /// <summary>
    /// Set element to exit as soon as entry is finished
    /// </summary>
    public void QueueExit()
    {
        if(Animating == true)
        {
            _queueExit = true;
        }
        else if(gameObject.activeSelf)
        {
            Exit();
        }
    }
    /// <summary>
    /// Cancel entry/exit and send straight to origin
    /// </summary>
    public void SetToOrigin()
    {
        if (!gameObject.activeSelf)
        {
            gameObject.SetActive(true);
        }

        foreach (UISlideInOut component in _subComponents)
        {
            component.SetToOrigin();
        }

        StopAllCoroutines();

        _rectTransform.anchoredPosition = _origin;
    }


    //Interactivity

    /// <summary>
    /// Sets the interactivity of a referenced ui component
    /// </summary>
    public void SetInteractive(bool interactive)
    {
        if(_interactive.Length > 0)
        {
            foreach (ControlInteractive component in _interactive)
            {
                component.SetEnabled(interactive);
            }
        }
    }
    /// <summary>
    /// Set the interactivity of all relevant ui components
    /// </summary>
    public void SetComponentsInteractive(bool interactive)
    {
        SetInteractive(interactive);

        foreach(UISlideInOut component in _subComponents)
        {
            component.SetInteractive(interactive);
        }
    }


    //Enter and exit coroutines

    /// <summary>
    /// Coroutine to carry out entrance transition
    /// </summary>
    IEnumerator CEnter()
    {
        //Uninteractive during transition
        if (!_hasOwner) SetComponentsInteractive(false);
        _animating = true;

        _rectTransform.anchoredPosition = _origin + _enterTravelDistance * _travelFrom.normalized;

        float time = 0f;

        while (time < _enterDelay)
        {
            time += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        time = 0f;

        while(time < _enterDuration)
        {
            time += Time.deltaTime;
            float halftime = time - Time.deltaTime / 2f;
            float velocity = -((_enterDuration - halftime) * (_enterDynamicSpeed / _enterDuration) + _enterBaseSpeed);
            _rectTransform.anchoredPosition3D += Time.deltaTime * velocity * _travelFrom.normalized;
            
            yield return new WaitForEndOfFrame();
        }

        //Set interactive again after transition
        if (!_hasOwner) SetComponentsInteractive(true);
        _animating = false;

        _rectTransform.anchoredPosition = _origin;

        EntranceFinished.Invoke();

        //Exit immediately if queued;
        if(_queueExit)
        {
            Exit();
        }
    }
    /// <summary>
    /// Coroutine to carry out exit transition
    /// </summary>
    IEnumerator CExit()
    {
        //Uninteractive during transition
        if (!_hasOwner) SetComponentsInteractive(false);
        _animating = true;

        _rectTransform.anchoredPosition = _origin;

        float time = 0f;

        while (time < _exitDelay)
        {
            time += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        time = 0f;

        while (time < _exitDuration)
        {
            time += Time.deltaTime;
            float halftime = time - Time.deltaTime / 2f;
            float velocity = (halftime / _exitDuration) * _exitDynamicSpeed + _exitBaseSpeed;
            _rectTransform.anchoredPosition3D += Time.deltaTime * velocity * _travelTo.normalized;
            
            yield return new WaitForEndOfFrame();
        }

        //Set interactive again after transition
        if (!_hasOwner) SetComponentsInteractive(true);
        _animating = false;

        ExitFinished.Invoke();
        ExitFinished.RemoveAllListeners();

        if (_disableOnExit)
        {
            gameObject.SetActive(false);
        }
    }


    //debug

    [Header("Editor use")]
    public bool _enter = false;
    public bool _exit = false;

    private void FixedUpdate()
    {
        if (_enter)
        {
            _enter = false;
            Enter();
        }
        if(_exit)
        {
            _exit = false;
            Exit();
        }
    }
}
