﻿using System.Collections.Generic;
using UnityEngine;

//Class used to swap between different menus with slide animators

public class MenuSwitcher : MonoBehaviour {

    //References
    [SerializeField] List<UISlideInOut> _menuPages;

    //Private variables
    int _currentPage;
    float _transitionStart;
    float _exitDuration;
    float _overlap;

    //Properties
    public UISlideInOut CurrentMenu
    {
        get
        {
            if(_menuPages != null && _menuPages.Count > _currentPage)
            {
                return _menuPages[_currentPage];
            }
            else
            {
                return null;
            }
        }
    }
    public bool IsCurrentMenuTransitioning
    {
        get
        {
            if (CurrentMenu == null) return false;
            return (CurrentMenu.Animating && CurrentMenu.gameObject.activeSelf);
        }
    }
    public int PageCount
    {
        get { return _menuPages.Count; }
    }
    public int Page
    {
        get { return _currentPage; }
    }

    //Initialisation

    /// <summary>
    /// On start, set _currentmenu equal to the first active menu
    /// </summary>
    private void Start()
    {
        _currentPage = PageCount;

        for(int i = 0; i < _menuPages.Count; ++i)
        {
            if(_menuPages[i].gameObject.activeSelf)
            {
                _currentPage = i;
                break;
            }
        }
    }


    //Update

    /// <summary>
    /// If currentmenu is false and it is time for entrance, then tell current menu to enter
    /// </summary>
    private void FixedUpdate()
    {
        if(CurrentMenu != null && !CurrentMenu.gameObject.activeSelf)
        {
            if (Time.time >= _transitionStart + _exitDuration - _overlap)
            {
                CurrentMenu.Enter();
            }
        }
    }


    //Methods

    /// <summary>
    /// Causes the current menu to exit if already entered, and sets chosen menu to enter after exit
    /// </summary>
    /// <param name="menu">
    /// The menu to be opened
    /// </param>
    /// <param name="overlap">
    /// The overlap between the entrance and exit animations
    /// </param>
    /// <returns>
    /// Returns false if menu wasn't swapped due to being mid-transition
    /// </returns>
    public bool SwapToMenu(int menu, float overlap = 0f)
    {
        if (menu == _currentPage || IsCurrentMenuTransitioning)
        {
            return false;
        }
        else
        {
            //If current menu is active then transition out and start timer for entrance, otherwise replace current with new menu
            if(CurrentMenu == null)
            {
                _exitDuration = 0f;
                _overlap = overlap;
                _transitionStart = Time.time;
            }
            else if (CurrentMenu.gameObject.activeSelf)
            {
                _exitDuration = CurrentMenu.ExitLength;
                _overlap = overlap;
                _transitionStart = Time.time;

                CurrentMenu.Exit();
            }
            else
            {
                _overlap = overlap;
            }

            _currentPage = menu;    //Will be swapped to after timer expires via fixedupdate

            return true;
        }
    }
    /// <summary>
    /// Close all menus, run followingmethod on menu closed
    /// </summary>
    /// <returns>
    /// Returns false if couldn't close due to transition
    /// </returns>
    public bool CloseMenu(bool force = true, UnityEngine.Events.UnityAction followingMethod = null)
    {
        if (CurrentMenu != null && !IsCurrentMenuTransitioning)
        {
            if (followingMethod != null)
            {
                CurrentMenu.ExitFinished.AddListener(followingMethod);
            }

            CurrentMenu.Exit();
            _currentPage = PageCount;

            return true;
        }
        else if(CurrentMenu != null && force)
        {
            if (followingMethod != null)
            {
                CurrentMenu.ExitFinished.AddListener(followingMethod);
            }

            CurrentMenu.QueueExit();
            _currentPage = PageCount;

            return true;
        }
        else if(CurrentMenu == null)
        {
            if (followingMethod != null) followingMethod.Invoke();
            return true;
        }
        else
        {
            return false;
        }
    }


    //Pagelist editing

    /// <summary>
    /// Add a new page to the end of the list
    /// </summary>
    public void AddPage(UISlideInOut slider)
    {
        _menuPages.Add(slider);
    }
    /// <summary>
    /// Remove all entries from the list
    /// </summary>
    public void EmptyList()
    {
        _menuPages = new List<UISlideInOut>();
        _currentPage = PageCount;
    }

}
