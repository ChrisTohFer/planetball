﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//

public class UIAnimator : MonoBehaviour {

    //References

    Animator _animator;

    //Initialisation

    private void OnEnable()
    {
        _animator = GetComponent<Animator>();
    }

    //Methods

    public void SetBoolTrue(string boolName)
    {
        _animator.SetBool(boolName, true);
    }
    public void SetBoolFalse(string boolName)
    {
        _animator.SetBool(boolName, false);
    }


}
