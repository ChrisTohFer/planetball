﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class GameSettingSlider : MonoBehaviour {

    //References
    [SerializeField] BoxSlider _slider;

    //Properties
    [SerializeField] GameSettings.Type _id;

    //Private
    GameSettings.FloatSetting _setting;
    

    //Initialisation

    /// <summary>
    /// Use id to get access to the relevant setting, set slider bounds
    /// </summary>
    private void OnEnable()
    {
        _setting = GameSettings.GetSetting(_id);

        _slider.minValue = _setting.Min;
        _slider.maxValue = _setting.Max;
        
        SetToCurrentValue();

        GameSettings.OnSync.AddListener(SetToCurrentValue);
    }
    //
    private void OnDisable()
    {
        GameSettings.OnSync.RemoveListener(SetToCurrentValue);
    }


    //Value setting methods

    /// <summary>
    /// Set the slider value to the current value in options
    /// </summary>
    public void SetToCurrentValue()
    {
        _slider.Value = _setting.Value;
    }
    /// <summary>
    /// Set the slider value to the default value
    /// </summary>
    public void SetDefault()
    {
        _slider.Value = _setting.Default;
    }
    /// <summary>
    /// Set the value of the slider equal to other slider if value is greater
    /// </summary>
    public void SetValueIfGreater(GameSettingSlider other)
    {
        if (_setting != null && other._setting != null && other._slider.Value > _slider.Value)
        {
            _slider.Value = other._slider.Value;
        }
    }
    /// <summary>
    /// Set the value of the slider equal to other slider if value is lesser
    /// </summary>
    public void SetValueIfLesser(GameSettingSlider other)
    {
        if (_setting != null && other._setting != null && other._slider.Value < _slider.Value)
        {
            _slider.Value = other._slider.Value;
        }
    }

    //Callbacks

    /// <summary>
    /// On value change, update options
    /// </summary>
    public void OnValueChanged()
    {
        _setting.Value = _slider.Value;
    }

}
