﻿using UnityEngine;
using TMPro;

//Class controls the appearance (length and text) of a health bar

public class HealthBar : MonoBehaviour {

    public RectTransform BarTransform;
    public TextMeshProUGUI Counter;

    float _fraction = 1f;
    float _targetFraction = 1f;

    float _startSize;

    private void OnEnable()
    {
        _startSize = BarTransform.sizeDelta.x;

        Counter.text = string.Format("{0:#,##0}", 10000000000);
    }

    public void SetNewHealth(float fraction)
    {
        _targetFraction = fraction;

    }

    private void Update()
    {
        if(_fraction > _targetFraction)
        {
            _fraction -= Time.deltaTime * (0.05f + 0.4f * (_fraction - _targetFraction));
            if(_fraction < _targetFraction)
            {
                _fraction = _targetFraction;
            }

            BarTransform.sizeDelta = new Vector2(_startSize * _fraction, BarTransform.sizeDelta.y);//  SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, fraction * 100f);
            Counter.text = string.Format("{0:#,0}", 10000000000 * _fraction);

        }
    }

}
