﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Events;

public class UICounter : MonoBehaviour {

    //References
    [SerializeField] TMP_InputField _inputField;

    //Variables
    public int MinValue = 1;
    public int MaxValue = 1;

    //Private variables
    int _value;

    //Property access
    public int Value
    {
        get { return _value; }
        set { SetTextValue(ClampValue(value)); }
    }

    //Events
    [System.Serializable] public class IntEvent : UnityEvent<int> { }
    public IntEvent ValueChanged;


    //Initialisation

    private void OnEnable()
    {
        int textValue = ReadValue();
        _value = ClampValue(textValue);
        if(_value != textValue)
        {
            SetTextValue(_value);
        }
    }


    //Methods

    /// <summary>
    /// Returns the value in the textbox
    /// </summary>
    int ReadValue()
    {
        return int.Parse(_inputField.text);
    }
    /// <summary>
    /// Clamp specified value between min and max
    /// </summary>
    int ClampValue(int value)
    {
        return Mathf.Clamp(value, MinValue, MaxValue);
    }
    /// <summary>
    /// Set the value in the text box to the passed in value
    /// </summary>
    void SetTextValue(int value)
    {
        _inputField.text = value.ToString();
    }
    

    //Callbacks

    /// <summary>
    /// When value changes, read the new value
    /// </summary>
    public void OnValueChanged()
    {
        int textValue = ReadValue();

        //If value is invalid, set to a valid value and wait for next callback to change anything
        if(textValue < MinValue || textValue > MaxValue)
        {
            textValue = ClampValue(textValue);
            SetTextValue(textValue);
        }
        else if(textValue != _value)
        {
            _value = textValue;
            ValueChanged.Invoke(_value);
        }
    }
    /// <summary>
    /// Increment the value + update the text
    /// </summary>
    public void IncrementValue()
    {
        if(_value + 1 <= MaxValue)
        {
            SetTextValue(_value + 1);
            OnValueChanged();
        }
    }
    /// <summary>
    /// Decrement the value
    /// </summary>
    public void DecrementValue()
    {
        if (_value - 1 >= MinValue)
        {
            SetTextValue(_value - 1);
            OnValueChanged();
        }
    }

}
