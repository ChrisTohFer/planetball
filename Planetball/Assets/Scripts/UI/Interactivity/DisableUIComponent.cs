﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

//Class is used to disable components and dim their appearance to be "Greyed out"

public class DisableUIComponent : MonoBehaviour {

    [Header("References")]
    [SerializeField] MonoBehaviour[] _components;
    [SerializeField] TextMeshProUGUI[] _textComponents;
    [SerializeField] Image[] _imageComponents;

    [Header("Properties")]
    [Tooltip("The percentage of base alpha which the components will be dimmed to when disabled")]
    [SerializeField] float _dimFraction;

    bool _enabled = true;

    public void EnableComponents()
    {
        if(!_enabled)
        {
            _enabled = true;

            foreach(MonoBehaviour mb in _components)
            {
                mb.enabled = true;
            }
            foreach(TextMeshProUGUI tm in _textComponents)
            {
                Color c = tm.color;
                c.a = c.a / (1f - _dimFraction);
                tm.color = c;
            }
            foreach(Image i in _imageComponents)
            {
                Color c = i.color;
                c.a = c.a / (1f - _dimFraction);
                i.color = c;
            }
        }
    }
    public void DisableComponents()
    {
        if (_enabled)
        {
            _enabled = false;

            foreach (MonoBehaviour mb in _components)
            {
                mb.enabled = false;
            }
            foreach (TextMeshProUGUI tm in _textComponents)
            {
                Color c = tm.color;
                c.a = c.a * _dimFraction;
                tm.color = c;
            }
            foreach (Image i in _imageComponents)
            {
                Color c = i.color;
                c.a = c.a * _dimFraction;
                i.color = c;
            }
        }
    }

}
