﻿using UnityEngine;
using TMPro;

//Base class for wrapping the interactable property of textfields

public class TextFieldInteractive : ControlInteractive
{
    [SerializeField] TMP_InputField _field;

    public override void SetEnabled(bool enabled)
    {
        if (!_disableOnClient || !IsClient)
        {
            _field.interactable = enabled;
        }
        else
        {
            _field.interactable = false;
        }
    }
}
