﻿using UnityEngine;
using UnityEngine.UI;

//Base class for wrapping the interactable property of buttons

public class ButtonInteractive : ControlInteractive
{
    [SerializeField] Button _button;

    public override void SetEnabled(bool enabled)
    {
        if(!_disableOnClient || !IsClient)
        {
            _button.interactable = enabled;
        }
        else
        {
            _button.interactable = false;
        }
    }
}
