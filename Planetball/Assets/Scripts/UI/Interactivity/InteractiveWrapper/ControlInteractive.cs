﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Script provides a "wrapper" to the interactive property of ui components in order to better control it

public abstract class ControlInteractive : MonoBehaviour {

    //Properties

    [SerializeField] protected bool _disableOnClient = false;

    protected bool IsClient
    {
        get { return BoltNetwork.IsRunning && !BoltNetwork.IsServer; }
    }

    //Initialisation

    private void OnEnable()
    {
        if(IsClient && _disableOnClient)
        {
            SetEnabled(false);
        }
    }


    //Methods

    public abstract void SetEnabled(bool enabled);

}
