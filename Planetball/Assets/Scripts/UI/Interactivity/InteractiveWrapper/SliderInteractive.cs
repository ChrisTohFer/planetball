﻿using UnityEngine;
using UnityEngine.UI;

//Base class for wrapping the interactable property of sliders

public class SliderInteractive : ControlInteractive
{
    [SerializeField] Slider _slider;

    public override void SetEnabled(bool enabled)
    {
        if (!_disableOnClient || !IsClient)
        {
            _slider.interactable = enabled;
        }
        else
        {
            _slider.interactable = false;
        }
    }
}