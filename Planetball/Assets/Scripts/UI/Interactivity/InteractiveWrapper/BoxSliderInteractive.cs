﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Class provides a wrapper for box slider interactivity

public class BoxSliderInteractive : ControlInteractive
{
    [SerializeField] Slider _slider;
    [SerializeField] Button _leftArrow;
    [SerializeField] Button _rightArrow;

    public override void SetEnabled(bool enabled)
    {
        if (!_disableOnClient || !IsClient)
        {
            _slider.interactable = enabled;
            _leftArrow.interactable = enabled;
            _rightArrow.interactable = enabled;
        }
        else
        {
            _slider.interactable = false;
            _leftArrow.interactable = false;
            _rightArrow.interactable = false;
        }
    }
}
