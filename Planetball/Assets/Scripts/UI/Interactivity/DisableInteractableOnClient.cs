﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class simply sets interactable ui components to be non-interactive when on a client
/// </summary>
public class DisableInteractableOnClient : MonoBehaviour {
    
    private void OnEnable()
    {
        bool interactable = BoltNetwork.IsServer;

        Button b = GetComponentInChildren<Button>();
        if(b != null) b.interactable = interactable;

        Slider s = GetComponentInChildren<Slider>();
        if(s != null) s.interactable = interactable;
        

    }

}
