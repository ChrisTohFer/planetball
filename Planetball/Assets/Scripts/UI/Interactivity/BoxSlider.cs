﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

//Class controls the appearance and functionality of a whole-number slider with a box appearance

public class BoxSlider : MonoBehaviour {


    //References

    [Header("References")]
    [SerializeField] Slider _slider;
    [SerializeField] HorizontalLayoutGroup _boxLayout;
    [SerializeField] RectTransform _sliderArea;

    //Resources

    [Header("Resources")]
    [SerializeField] GameObject _boxPrefab;
    [SerializeField] Sprite _emptyBox;
    [SerializeField] Sprite _filledBox;

    //Properties

    [Header("Properties")]
    [SerializeField] int _increments;
    public float minValue;
    public float maxValue;
    [SerializeField] float _boxWidth = 24.3f;
    [SerializeField] float _boxHeight = 45.45f;
    [SerializeField] float _boxSpacing = 0f;
    [SerializeField] bool _fillOneOnly = false;

    //Private references and variables

    Image[] _boxes;
    int _value;
    bool _initialised = false;

    //Accessible properties
    public float Value
    {
        get
        {
            return minValue + ((float)_value / Mathf.Max(_increments - 1, 1)) * (maxValue - minValue);
        }
        set
        {
            Initialize();
            
            float difference = Mathf.Abs(value - minValue);
            int increment = 0;

            for (int i = 1; i < _increments; ++i)
            {
                if (Mathf.Abs(value - ValueAtIncrement(i)) < difference)
                {
                    increment = i;
                    difference = Mathf.Abs(value - ValueAtIncrement(i));
                }
            }

            _value = increment;
            _slider.value = _value;
        }
    }
    public int IntValue
    {
        get { return Mathf.RoundToInt(Value); }
    }
    public int Increment
    {
        get { return _value; }
    }

    //Events
    [System.Serializable] public class FloatEvent : UnityEvent<float> { }
    [System.Serializable] public class IntEvent : UnityEvent<int> { }

    public FloatEvent ValueChangedFloat;
    public IntEvent ValueChangedInt;


    //Initialisation

    //
    private void OnEnable()
    {
        Initialize();
    }
    /// <summary>
    /// Create boxes to fill bar
    /// </summary>
    public void Initialize()
    {
        if (!_initialised)
        {
            ClearBoxes();

            //Initialise boxes
            _boxes = new Image[_increments];

            for (int i = 0; i < _increments; ++i)
            {
                GameObject g = Instantiate(_boxPrefab, _boxLayout.transform);
                _boxes[i] = g.GetComponent<Image>();
                _boxes[i].sprite = _emptyBox;
                _boxes[i].rectTransform.sizeDelta = new Vector2(_boxWidth, _boxHeight);
            }

            //Edit values of layout
            _boxLayout.spacing = _boxSpacing;

            //Set slider size and max value
            _slider.maxValue = _increments - 1;

            Vector2 sliderArea = new Vector2((_boxWidth + _boxSpacing) * (_increments - 1), _boxHeight);

            GetComponent<RectTransform>().sizeDelta = new Vector2(sliderArea.x + _boxWidth, sliderArea.y);
            
            _sliderArea.GetComponent<RectTransform>().sizeDelta = sliderArea;
            _sliderArea.anchoredPosition = new Vector2((_boxWidth + _boxSpacing) * 0.5f, 0f);

            _initialised = true;
        }

        UpdateAppearance();
    }
    /// <summary>
    /// Reinitialise (used if need to initialise externally at a particular point in time)
    /// </summary>
    public void ReInitialize()
    {
        _initialised = false;
        Initialize();
    }
    /// <summary>
    /// Clear all boxes
    /// </summary>
    private void ClearBoxes()
    {
        for (int i = _boxLayout.transform.childCount - 1; i >= 0; --i)
        {
            Destroy(_boxLayout.transform.GetChild(i).gameObject);
        }
    }


    //Change value

    /// <summary>
    /// Increase the value of the slider by one
    /// </summary>
    public void IncrementValue()
    {
        _value++;
        if (_value >= _increments) _value = _increments - 1;

        _slider.value = _value;
    }
    /// <summary>
    /// Decrease the value of the slider by one
    /// </summary>
    public void DecrementValue()
    {
        _value--;
        if (_value < 0) _value = 0;

        _slider.value = _value;
    }


    //Value access

    //
    float ValueAtIncrement(int increment)
    {
        return minValue + ((float)increment / Mathf.Max(_increments - 1, 1)) * (maxValue - minValue);
    }


    //Callbacks

    /// <summary>
    /// When value is changed, update the appearance of the boxes
    /// </summary>
    public void OnValueChanged()
    {
        _value = Mathf.RoundToInt(_slider.value);
        
        ValueChangedFloat.Invoke(Value);
        ValueChangedInt.Invoke(IntValue);

        UpdateAppearance();
    }
    /// <summary>
    /// Update the appearance of the boxes
    /// </summary>
    public void UpdateAppearance()
    {
        for (int i = 0; i < _increments; ++i)
        {
            if (i == _value)
            {
                _boxes[i].sprite = _filledBox;
            }
            else if (i < _value && !_fillOneOnly)
            {
                _boxes[i].sprite = _filledBox;
            }
            else
            {
                _boxes[i].sprite = _emptyBox;
            }
        }
    }


}
