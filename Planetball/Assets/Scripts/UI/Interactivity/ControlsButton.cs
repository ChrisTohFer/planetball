﻿using UnityEngine;
using TMPro;

using System.Collections.Generic;
using System;

public class ControlsButton : MonoBehaviour {

    //List of active controlsbuttons
    private static List<ControlsButton> _buttons;
    
    //References
    [SerializeField] TextMeshProUGUI _textObject;

    //Properties
    [SerializeField] string _tag;  //Tag for the control this button changes

    //Private properties
    private KeyCode Key;
    private bool _active = false;


    //Enable/Disable

    /// <summary>
    /// Add button to list, get key and set text
    /// </summary>
    private void OnEnable()
    {
        if (_buttons == null)
            _buttons = new List<ControlsButton>();
        _buttons.Add(this);
        GetKey();
        SetText();
    }
    /// <summary>
    /// Remove button from list on disable
    /// </summary>
    private void OnDisable()
    {
        _buttons.Remove(this);
    }

    
    //Selection

    /// <summary>
    /// If a control button is active, deselect it
    /// </summary>
    public static void DeselectAll()
    {
        if(_buttons != null)
        {
            foreach (ControlsButton cb in _buttons)
            {
                cb.Deselect();
            }
        }
    }
    /// <summary>
    /// Deselect other buttons and select this one
    /// Change appearance appropriately
    /// </summary>
    public void Select()
    {
        DeselectAll();

        _textObject.text = "PRESS KEY";

        _active = true;
    }
    /// <summary>
    /// Deselect this button
    /// </summary>
    public void Deselect()
    {
        SetText();

        _active = false;
    }


    //Update stuff

    /// <summary>
    /// When active, check to see if a key is pressed down
    /// </summary>
    private void Update()
    {
        if(_active)
        {
            foreach(KeyCode ck in Enum.GetValues(typeof(KeyCode)))
            {
                if(Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Mouse0))
                {
                    Deselect();
                    break;
                }
                if(Input.GetKeyDown(ck))
                {
                    Key = ck;
                    SetKey();
                    Deselect();
                    break;
                }
            }
        }
    }


    //Get and set the relevant control keycode

    /// <summary>
    /// Get the key from the controls singleton class
    /// </summary>
    public void GetKey()
    {
        Key = Controls.GetKey(_tag);
    }
    /// <summary>
    /// Set the key in the controls singleton class
    /// </summary>
    public void SetKey()
    {
        Controls.SetKey(_tag, Key);
    }
    /// <summary>
    /// Set the text of the button to the current key code
    /// </summary>
    public void SetText()
    {
        _textObject.text = Key.ToString().ToUpper();
    }
    

}
