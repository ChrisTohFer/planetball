﻿using UnityEngine;
using TMPro;

//Causes a textmeshpro renderer to flash

public class FlashingText : MonoBehaviour {

    [SerializeField] TextMeshProUGUI _textObject;
    [SerializeField] float _period;
    [SerializeField] float _minAlpha;
    [SerializeField] bool _alphaRoot;
    [SerializeField] float _delay;

    float _phase = 0f;

    private void OnEnable()
    {
        Color color = _textObject.color;
        color.a = _minAlpha;
        _textObject.color = color;
    }

    private void FixedUpdate()
    {
        if(_delay <= 0f)
        {
            _phase = Mathf.Repeat(_phase + Time.fixedDeltaTime * 2f * Mathf.PI / _period, 2f * Mathf.PI);
            float alpha = _minAlpha + (1f - _minAlpha) * (1f - Mathf.Cos(_phase)) / 2f;
            if (_alphaRoot) { alpha = Mathf.Sqrt(alpha); }

            Color color = _textObject.color;
            color.a = alpha;
            _textObject.color = color;
        }
        else
        {
            _delay -= Time.fixedDeltaTime;
        }
    }

}
