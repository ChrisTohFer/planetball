﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Causes gameobject to grow to its normal size after enabling

public class GrowOnEnable : MonoBehaviour {

    [SerializeField] float _startScale = 0f;
    [SerializeField] float _scale = 1f;
    [SerializeField] float _period = 1f;


    //Enable

    private void OnEnable()
    {
        StartCoroutine(GrowToScale());
    }

    //Coroutine

    IEnumerator GrowToScale()
    {
        float scale = _startScale;
        transform.localScale = new Vector3(scale, scale, scale);

        while (scale < 1f)
        {
            yield return new WaitForEndOfFrame();
            scale += (_scale - _startScale) * Time.deltaTime / _period;
            transform.localScale = new Vector3(scale, scale, scale);
        }

        if(scale > 1f)
        {
            transform.localScale = new Vector3(scale, scale, scale);
        }
    }


}
