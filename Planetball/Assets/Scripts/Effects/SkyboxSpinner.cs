﻿using UnityEngine;

//Script rotates the skybox at AngularVelocity degrees per second

public class SkyboxSpinner : MonoBehaviour {

    [Header("Properties")]
    public float AngularVelocity;

    private static float _angle = 0f;   //Static private float to maintain angle across scenes

    private void Update()
    {
        IncrementAngle();
        RenderSettings.skybox.SetFloat("_Rotation", _angle);
    }
    private void IncrementAngle()
    {
        _angle += AngularVelocity * Time.deltaTime;
        if(_angle > 360)
            _angle -= 360;
    }
}
