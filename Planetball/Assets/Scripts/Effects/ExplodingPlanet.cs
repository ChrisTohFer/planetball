﻿using System.Collections;
using UnityEngine;

public class ExplodingPlanet : MonoBehaviour {

    [Header("References")]
    public Rigidbody[] Components;
    public RandomSound ExplosionSounds;

    [Header("Properties")]
    public float SpeedVariation = 1f;
    public float LifeTime = 4f;
    public float ShrinkTime = 2f;
    public float BitRotationMax = 6f;
    
    public void Explode(Vector3 velocity)
    {
        gameObject.SetActive(true);

        foreach(Rigidbody rb in Components)
        {
            rb.velocity = velocity + Random.onUnitSphere * SpeedVariation;

            float rotX = BitRotationMax * 2f * (Random.value - 0.5f);
            float rotY = BitRotationMax * 2f * (Random.value - 0.5f);
            float rotZ = BitRotationMax * 2f * (Random.value - 0.5f);

            rb.angularVelocity = new Vector3(rotX, rotY, rotZ);
        }

        //ExplosionSounds.Volume = Mathf.Sqrt(transform.localScale.x / 3f);
        ExplosionSounds.PlayRandomSound();

        StartCoroutine(ShrinkAndDestroy());

    }

    private IEnumerator ShrinkAndDestroy()
    {
        yield return new WaitForSeconds(LifeTime - ShrinkTime);

        float scale = 1f;
        while(scale > 0f)
        {
            scale -= Time.deltaTime / ShrinkTime;
            for(int i = 0; i < transform.childCount; ++i)
            {
                transform.GetChild(i).localScale = new Vector3(scale, scale, scale);
            }

            yield return new WaitForEndOfFrame();
        }

        Destroy(gameObject);
    }

}
