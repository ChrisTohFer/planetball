﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Class provides global access to camerashake (and potentially other similar methods)

public class Juce : MonoBehaviour {

    //
    public static Juce singleton;

    private void Awake()
    {
        singleton = this;
    }

    /// <summary>
    /// Changes the speed of the game to newSpeed for duration seconds
    /// </summary>
    public void ChangeTimeScale(float newSpeed, float duration)
    {
        StartCoroutine(CChangeTimeScale(newSpeed, duration));
    }
    public IEnumerator CChangeTimeScale(float newSpeed, float duration)
    {
        Time.timeScale = newSpeed;

        yield return new WaitForSeconds(duration);

        Time.timeScale = 1f;
    }
    /// <summary>
    /// Move camera y according to a sin function with the assigned parameters
    /// </summary>
    public void CameraShake(float intensity, float speed, float duration)
    {
        StartCoroutine(CCameraShake(intensity, speed, duration));
    }
    
    public IEnumerator CCameraShake(float intensity, float speed, float duration)
    {
        Transform cam = Camera.main.transform;
        float time = 0;

        while (time < duration)
        {
            time += Time.deltaTime / Time.timeScale;
            cam.position = new Vector3(cam.position.x, intensity * Mathf.Sin(time * speed), cam.position.z);
            yield return new WaitForEndOfFrame();
        }
    }

}
